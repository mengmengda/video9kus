//
//  NSDate+Helper.m
//  Video9kus
//
//  Created by Lincal on 2017/4/16.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "NSDate+Helper.h"

@implementation NSDate (Helper)

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format {
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:format];
    NSDate *date = [inputFormatter dateFromString:string];
    return date;
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format {
    return [date stringWithFormat:format];
}

static NSDateFormatter * outputFormatter;

- (NSString *)stringWithFormat:(NSString *)format {
    if(outputFormatter == nil) outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:format];
    NSString *timestamp_str = [outputFormatter stringFromDate:self];
    return timestamp_str;
}

+ (NSString *)unixStamp2TimeIntervalSinceNow:(double)unixStamp {
    NSString *retString;
    long unixTimeInterval = unixStamp;
    long currentTimeInterval = [[NSDate date] timeIntervalSince1970];
    long timeIntervalSinceNow = currentTimeInterval - unixTimeInterval;
    if (timeIntervalSinceNow < 0) {
        return nil;
    }
    if (timeIntervalSinceNow < 60) {
        retString = [NSString stringWithFormat:@"%ld秒", timeIntervalSinceNow];
    } else if (timeIntervalSinceNow < 60*60) {
        retString = [NSString stringWithFormat:@"%ld分钟", timeIntervalSinceNow/60];
    } else if (timeIntervalSinceNow < 60*60*24) {
        retString = [NSString stringWithFormat:@"%ld小时", timeIntervalSinceNow/(60*60)];
    } else {
        retString = [NSString stringWithFormat:@"%ld天", timeIntervalSinceNow/(60*60*24)];
    }
    
    return retString;
}

@end
