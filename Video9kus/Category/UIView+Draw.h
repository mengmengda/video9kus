//
//  UIView+Draw.h
//  Lincal
//
//  Created by Lincal on 16/8/31.
//  Copyright © 2016年 Lincal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Draw)

- (UIView *)drawBounderWidth:(CGFloat)width Color:(UIColor *)color;
- (UIView *)drawBounderWidth:(CGFloat)width radius:(CGFloat)radius Color:(CGColorRef )color;

@end

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@interface UILabel (ErrorShow)

- (void)showErrorAnimationsWithText:(NSString *)text;

@end

