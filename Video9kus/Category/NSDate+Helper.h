//
//  NSDate+Helper.h
//  Video9kus
//
//  Created by Lincal on 2017/4/16.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)

/// return NSDate with time string
+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;

/// return string with NSDate
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;

///return string which can describes the time before
+ (NSString *)unixStamp2TimeIntervalSinceNow:(double)unixStamp;

@end
