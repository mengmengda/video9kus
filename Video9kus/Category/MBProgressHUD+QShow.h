//
//  MBProgressHUD+QuickShow.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

#define MBProMessageShowTimeLength 2.0f

@interface MBProgressHUD (QShow)

+ (void)showSuccess:(NSString *)success;
+ (void)showError:(NSString *)error;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;
+ (void)showError:(NSString *)error toView:(UIView *)view;

+ (MBProgressHUD *)showMessage:(NSString *)message;
+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view;
+ (MBProgressHUD *)showMessage:(NSString *)message toViewCanTap:(UIView *)view;

+ (void)showLoadingWithMessage:(NSString *)message toView:(UIView *)view;

+ (void)showCustomLoadingWithMessage:(NSString *)message toView:(UIView *)view;

+ (void)hideHUDForView:(UIView *)view;
+ (void)hideHUD;

@end

