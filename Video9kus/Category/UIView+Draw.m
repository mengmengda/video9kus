//
//  UIView+Draw.m
//  Lincal
//
//  Created by Lincal on 16/8/31.
//  Copyright © 2016年 Lincal. All rights reserved.
//

#import "UIView+Draw.h"

@implementation UIView (Draw)

- (UIView *)drawBounderWidth:(CGFloat)width  Color:(UIColor *)color {
    [self.layer setBorderWidth:width];
    [self.layer setBorderColor:color.CGColor];
    return self;
}

- (UIView *)drawBounderWidth:(CGFloat)width radius:(CGFloat)radius Color:(CGColorRef)color {
    [self.layer setBorderWidth:width];
    [self.layer setBorderColor:color];
    [self.layer setCornerRadius:radius];
    self.layer.masksToBounds = YES;
    return self;
}

@end


@implementation UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

@implementation UILabel (ErrorShow)

- (void)showErrorAnimationsWithText:(NSString *)text{
    
    CAKeyframeAnimation *shakeAnim = [CAKeyframeAnimation animation];
    shakeAnim.keyPath = @"transform.translation.x";
    shakeAnim.duration = 0.15;
    CGFloat delta = 10;
    shakeAnim.values = @[@0 , @(-delta), @(delta), @0];
    shakeAnim.repeatCount = 2;
    [self.layer addAnimation:shakeAnim forKey:nil];
    
    self.text = text;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
    }];
    
}

@end
