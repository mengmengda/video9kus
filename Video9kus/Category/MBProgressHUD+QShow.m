//
//  MBProgressHUD+QuickShow.m
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MBProgressHUD+QShow.h"

@implementation MBProgressHUD (QShow)

#pragma mark ------------ < Custom > ------------
+ (void)showSuccess:(NSString *)success
{
    [self showSuccess:success toView:nil];
}

+ (void)showError:(NSString *)error
{
    [self showError:error toView:nil];
}

+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:@"error.png" view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:@"success.png" view:view];
}

+ (void)show:(NSString *)text
        icon:(NSString *)icon
        view:(UIView *)view
{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = text;
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon]];
    hud.mode = MBProgressHUDModeCustomView;
    hud.removeFromSuperViewOnHide = YES;
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.3];
    hud.contentColor = [UIColor whiteColor];
    hud.label.font = [UIFont systemFontOfSize:13];
    [hud hideAnimated:YES afterDelay:1];
}

#pragma mark ------------ < Message > ------------
+ (MBProgressHUD *)showMessage:(NSString *)message
{
    return [self showMessage:message toView:nil];
}

+ (MBProgressHUD *)showMessage:(NSString *)message toViewCanTap:(UIView *)view {
    MBProgressHUD *hud = [self showMessage:message toView:view];
    hud.userInteractionEnabled = NO;
    return hud;
}

+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = message;
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.3];
    hud.contentColor = [UIColor whiteColor];
    hud.label.font = [UIFont systemFontOfSize:13];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:MBProMessageShowTimeLength];
    return hud;
}

#pragma mark ------------ < Loading > ------------
+ (void)showLoadingWithMessage:(NSString *)message toView:(UIView *)view{
    
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.3];
    hud.contentColor = [UIColor whiteColor];
    hud.label.font = [UIFont systemFontOfSize:13];
    hud.label.text = message==nil?@"Loading":message;
    
}

const static int kImgCount = 26;
#define imgFontName @"s"

+ (void)showCustomLoadingWithMessage:(NSString *)message toView:(UIView *)view{
    
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = message;
    hud.mode = MBProgressHUDModeCustomView;
    
    UIImageView * imgV = [[UIImageView alloc] init];
    NSMutableArray * imgs = [NSMutableArray array];
    for (int i = 1; i <= kImgCount; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%d", imgFontName, i]];
        [imgs addObject:[self oriImage:image scaleToSize:CGSizeMake(37, 37)]];
    }
    imgV.animationImages = imgs;
    imgV.animationDuration = 1;
    [imgV startAnimating];

    hud.customView = imgV;
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.3];
    hud.contentColor = [UIColor whiteColor];
    hud.label.font = [UIFont systemFontOfSize:13];
    hud.animationType = MBProgressHUDAnimationFade;
    
}

+ (UIImage*)oriImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

#pragma mark ------------ < Hide > ------------
+ (void)hideHUDForView:(UIView *)view
{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    [self hideHUDForView:view animated:YES];
}

+ (void)hideHUD
{
    [self hideHUDForView:nil];
}

@end
