//
//  main.m
//  Video9kus
//
//  Created by Lincal on 17/3/30.
//  Copyright (c) 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
