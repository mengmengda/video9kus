//
//  MMDNetworkInterface.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#ifndef MMDNetworkInterface_h
#define MMDNetworkInterface_h

#pragma mark ------------ < Api joint > ------------

/// basic url after the gateway
#define MMDNetworkRequestBasicURL  [MMDNetworkOption sharedMMDNetworkOption].basicApiServerURL

/// joint url
#define kURL(...) [MMDNetworkRequestBasicURL stringByAppendingFormat:[__VA_ARGS__ stringByAppendingFormat:@""],nil]

#pragma mark ------------ < Api Address > ------------

#define kVisitorLogin @"/User/visitorLogin" //游客注册接口
#define kLogin        @"/User/login"        //登录

#define kVideoUpload  @"/Video/uploadVideo"   //上传我的视频
#define kVideoList    @"/Video/userVideoList" //我的视频列表
#define kVideoDelete  @"/Video/userVideoDel"  //删除我的视频
#define kVideoCheck   @"/Video/checkTransform"//查询视频结果

#endif /* MMDNetworkInterface_h */
