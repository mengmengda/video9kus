//
//  MMDNetworkConfig.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#ifndef MMDNetworkConfig_h
#define MMDNetworkConfig_h

// overall defult setting
#define MMDNetWork_AppTimestamp     @"timestamp"
#define MMDNetWork_VERSION          @"versionCode"
#define MMDNetwork_PID              @"pid"
#define MMDNetwork_PASS             @"pass"
#define MMDNetwork_UUID             @"UUID"

/// acceptable content type
#define MMDAcceptableContentType_ApplicationJson        @"application/json"
#define MMDAcceptableContentType_TextHtml               @"text/html"
#define MMDAcceptableContentType_TextJvascript          @"text/javascript"
#define MMDAcceptableContentType_TextJson               @"text/json"
#define MMDAcceptableContentType_TextPlain              @"text/plain"
#define MMDAcceptableContentType_urlencoded             @"application/x-www-form-urlencoded"

/// time-out time
#define MMDNewWork_timeoutInterValKey                   @"timeoutInterval"
#define MMDNetWork_TimeoutInterval                      15.f

#endif /* MMDNetworkConfig_h */
