//
//  NSString+MMDSecurity.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MMDSecurity)

/// encrypt str -> MD5
+ (NSString *)stringToMD5:(NSString *)str;

@end
