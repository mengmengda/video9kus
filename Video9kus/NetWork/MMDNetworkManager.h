//
//  MMDNetworkManager.h
//  Video9kus
//
//  Created by Lincal on 2017/4/10.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "MMDNetworkConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ _Nonnull MMDNetworkManagerUploadBlock) (id <AFMultipartFormData> _Nonnull formData);
typedef void (^_Nullable MMDNetworkManagerProgressBlock) (NSProgress * _Nullable progress);
typedef void (^_Nonnull MMDNetworkManagerSuccessBlock) (NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject);
typedef void (^_Nonnull MMDNetworkManagerFailureBlock) (NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error);

@interface MMDNetworkManager : AFHTTPSessionManager

/// GET
+ (nonnull NSURLSessionDataTask *)mmd_GET:(nonnull NSString *)URLString
                           parameters:(nullable id)parameters
                             progress:(MMDNetworkManagerProgressBlock)downloadProgressBlock
                              success:(MMDNetworkManagerSuccessBlock)successBlock
                              failure:(MMDNetworkManagerFailureBlock)failureBloc;

/// POST
+ (nonnull NSURLSessionDataTask *)mmd_POST:(nonnull NSString *)URLString
                                parameters:(nullable id)parameters
                                  progress:(MMDNetworkManagerProgressBlock)uploadProgressBlock
                                   success:(MMDNetworkManagerSuccessBlock)successBlock
                                   failure:(MMDNetworkManagerFailureBlock)failureBlock;

/// Upload
+ (nonnull NSURLSessionDataTask *)mmd_UPLOAD:(nonnull NSString *)URLString
                              parameters:(nullable id)parameters
                                fileData:(nonnull NSData *)fileData
               constructingBodyWithBlock:(MMDNetworkManagerUploadBlock)block
                                progress:(MMDNetworkManagerProgressBlock)uploadProgressBlock
                                 success:(MMDNetworkManagerSuccessBlock)successBlock
                                 failure:(MMDNetworkManagerFailureBlock)failureBlock;

/// json string transform to containers
+ (id)jsonStrToContainers:(NSString *)str;

@end

NS_ASSUME_NONNULL_END
