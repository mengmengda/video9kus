//
//  MMDNetworkOption.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "MMDNetworkInterface.h"

/// netWork environment  1 : Online  0 : Develop
#define MMDNetWorkEnvironment 0

/// online base url
#define OnLineURL    @"http://ios.ivikan.com"
/// develop base url
#define DevelopURL   @"http://10.10.1.10/data/video_webroot/video_ios_inf"

#if MMDNetWorkEnvironment
#define MMDBaseURL  OnLineURL
#else
#define MMDBaseURL  DevelopURL
#endif

/// get the environment option with this key
#define MMDNetworkEnvironmentSwitchOptionKey  @"MMDEnvironmentSwitchOptionKey"

typedef NS_ENUM(NSInteger, MMDNetworkEnvironmentSwitchOption) {
    
    /// Online
    MMDNetworkEnvironmentSwitchOptionOnline = 0,
    
    /// Test 01
    MMDNetworkEnvironmentSwitchOptionTest01 = 1,

};

#define MMDNetWorkNormalService @""

typedef NS_ENUM(NSInteger,MMDNetworkServiceType) {
    
    /// normal service
    MMDNetWorkNormalServiceType = 0,
    
};

@interface MMDNetworkOption : NSObject

singleton_interface(MMDNetworkOption);

/// server type .Please set this property when invoke network request
@property (nonatomic, assign) MMDNetworkServiceType networkSericeType;

/// current network environment switch option value
@property (nonatomic,assign) MMDNetworkEnvironmentSwitchOption currentNetworkEnvironmentSwitchOption;

/// server url
@property (nonatomic,strong) NSString * basicApiServerURL;


@end
