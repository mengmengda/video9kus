//
//  MMDNetworkOption.m
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDNetworkOption.h"

#define kBaseURL(...) [MMDBaseURL stringByAppendingFormat:[__VA_ARGS__ stringByAppendingFormat:@""],nil]

@implementation MMDNetworkOption

singleton_implementation(MMDNetworkOption);

#pragma mark ------------ < Get Method > ------------
- (MMDNetworkEnvironmentSwitchOption)currentNetworkEnvironmentSwitchOption{
    NSInteger currentE = [[NSUserDefaults standardUserDefaults] integerForKey:MMDNetworkEnvironmentSwitchOptionKey];
    return currentE;
}

- (NSString *)basicApiServerURL{
    return kBaseURL([self gatewayServiceType]);
}

#pragma mark ------------ < Interior Method> ------------
/// default is normal service
- (NSString *)gatewayServiceType{
    
    NSString * gatewayType;
    
    switch (self.networkSericeType) {
            
        case MMDNetWorkNormalServiceType:
            gatewayType = MMDNetWorkNormalService;
            break;
            
        default:
            gatewayType = MMDNetWorkNormalService;
            break;
    }
    return gatewayType;
    
}

/// default is online domain url
- (NSString *)domainApiServerURL {
    
    switch (self.currentNetworkEnvironmentSwitchOption) {
        case MMDNetworkEnvironmentSwitchOptionOnline:
            return OnLineURL;
            break;
            
        case MMDNetworkEnvironmentSwitchOptionTest01:
            return DevelopURL;
            break;
            
        default:
            return OnLineURL;
            break;
    }
}


@end
