//
//  MMDNetworkManager.m
//  Video9kus
//
//  Created by Lincal on 2017/4/10.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDNetworkManager.h"
#import "NSString+MMDSecurity.h"

@implementation MMDNetworkManager

#pragma mark ------------ < Open Method > ------------
+ (nonnull NSURLSessionDataTask *)mmd_GET:(nonnull NSString *)URLString
                               parameters:(nullable id)parameters
                                 progress:(MMDNetworkManagerProgressBlock)downloadProgressBlock
                                  success:(MMDNetworkManagerSuccessBlock)successBlock
                                  failure:(MMDNetworkManagerFailureBlock)failureBlock {
    
    
    AFHTTPSessionManager *manager = [MMDNetworkManager mmd_addHeader];
    
    DLOG_LINE(@"\nGET Req >>> URL : \n[%@] \nPara : \n[%@]", URLString, parameters);
    
    return [manager GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
        if (downloadProgressBlock) downloadProgressBlock(downloadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary * dic = [self jsonDataToDic:responseObject];
        DLOG_LINE(@"\nGET Rsp >>> Success \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, dic);
        if (successBlock) successBlock(task, dic);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        DLOG_LINE(@"\nGET Rsp >>> Failure \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, error);
        if (failureBlock) failureBlock(task, error);
        
    }];
}

+ (nonnull NSURLSessionDataTask *)mmd_POST:(nonnull NSString *)URLString
                            parameters:(nullable id)parameters
                              progress:(MMDNetworkManagerProgressBlock)uploadProgressBlock
                               success:(MMDNetworkManagerSuccessBlock)successBlock
                               failure:(MMDNetworkManagerFailureBlock)failureBlock{
   
    AFHTTPSessionManager *manager = [MMDNetworkManager mmd_addHeader];
    
    DLOG_LINE(@"\nPOST Req >>> URL : \n[%@] \nPara : \n[%@]", URLString, parameters);
    
    return [manager POST:URLString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        if (uploadProgressBlock) uploadProgressBlock(uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary * dic = [self jsonDataToDic:responseObject];
        DLOG_LINE(@"\nPOST Rsp >>> Success \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, dic);
        if (successBlock) successBlock(task, dic);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        DLOG_LINE(@"\nPOST Rsp >>> Failure \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, error);
        if (failureBlock) failureBlock(task, error);
        
    }];
    
}

+ (nonnull NSURLSessionDataTask *)mmd_UPLOAD:(nonnull NSString *)URLString
                                  parameters:(nullable id)parameters
                                    fileData:(nonnull NSData *)fileData
                   constructingBodyWithBlock:(MMDNetworkManagerUploadBlock)block
                                    progress:(MMDNetworkManagerProgressBlock)uploadProgressBlock
                                     success:(MMDNetworkManagerSuccessBlock)successBlock
                                     failure:(MMDNetworkManagerFailureBlock)failureBlock {
    
    AFHTTPSessionManager *manager = [MMDNetworkManager mmd_addHeader];
    
    DLOG_LINE(@"\nUPLOAD Req >>> URL : \n[%@] \nPara : \n[%@]", URLString, parameters);
    
    return [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        DLOG_LINE(@"\nUPLOAD Req >>> URL : \n[%@] \nPara!!!! : \n[%@]", URLString, formData);
        [formData appendPartWithFileData:fileData
                                    name:@"file"
                                fileName:@"videoFromiOS-Apr.mp4"
                                mimeType:@"mp4"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        DLOG_LINE(@"\nUPLOAD Rsp >>> uploadProgress \nprogress >>> Total %lld Comple %lld", uploadProgress.totalUnitCount, uploadProgress.completedUnitCount);
        if (uploadProgressBlock) uploadProgressBlock(uploadProgress);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary * dic = [self jsonDataToDic:responseObject];
        DLOG_LINE(@"\nUPLOAD Rsp >>> Success \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, dic);
        if (successBlock) successBlock(task, dic);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        DLOG_LINE(@"\nUPLOAD Rsp >>> Failure \nOriURL >>> [ %@ ]\n%@", task.originalRequest.URL, error);
        if (failureBlock) failureBlock(task, error);
        
    }];
}

#pragma mark ------------ < Interior Method > ------------
+ (AFHTTPSessionManager *)mmd_addHeader{
    
    AFHTTPSessionManager *manager = [self addHeader];
    
    /// timestamp
    long long totalMilliseconds = [self dateTimeToMilliSeconds];
    NSString *timestamp = [NSString stringWithFormat:@"%lld", totalMilliseconds];
    
    /// uuid
    NSString * uuid = MMDUserUDID;
    
    /// pass
    NSString *md5Pass = [NSString stringToMD5:[NSString stringWithFormat:@"%@%@%@%@%@",MMDAPPPID,uuid,MMDAppVersion,timestamp,@"videoiOS"]];
    
    /// custom setting
    [manager.requestSerializer setValue:timestamp forHTTPHeaderField:MMDNetWork_AppTimestamp];
    [manager.requestSerializer setValue:MMDAPPPID forHTTPHeaderField:MMDNetwork_PID];
    [manager.requestSerializer setValue:MMDAppVersion forHTTPHeaderField:MMDNetWork_VERSION];
    [manager.requestSerializer setValue:uuid forHTTPHeaderField:MMDNetwork_UUID];
    [manager.requestSerializer setValue:md5Pass forHTTPHeaderField:MMDNetwork_PASS];
    
    return manager;
}

+ (AFHTTPSessionManager *)addHeader{
    
    static AFHTTPSessionManager * manager = nil;
    static dispatch_once_t oneToken;
    
    /// one-off init
    dispatch_once(&oneToken, ^{
        manager = [AFHTTPSessionManager manager];
        
        /// basic setting
        [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:
                                                               MMDAcceptableContentType_ApplicationJson,MMDAcceptableContentType_TextHtml,
                                                               MMDAcceptableContentType_TextJvascript,MMDAcceptableContentType_TextJson,
                                                               MMDAcceptableContentType_TextPlain,MMDAcceptableContentType_urlencoded,
                                                               nil]];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        // time-out setup
        [manager.requestSerializer willChangeValueForKey:MMDNewWork_timeoutInterValKey];
        manager.requestSerializer.timeoutInterval = MMDNetWork_TimeoutInterval;
        [manager.requestSerializer didChangeValueForKey:MMDNewWork_timeoutInterValKey];
        
    });
    
    return manager;
}

+ (long long) dateTimeToMilliSeconds {
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    long long totalMilliseconds = interval * 1000 ;
    return totalMilliseconds;
}

+ (NSDictionary *)jsonDataToDic:(id)response{
    NSData * data = response;
    return (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}

+ (id)jsonStrToContainers:(NSString *)str{
    NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}

@end
