//
//  MMDSwitchEnvironmentView.m
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDSwitchEnvironmentView.h"

@implementation MMDSwitchEnvironmentView

+(MMDSwitchEnvironmentView *)DefaultView{
    return [[self alloc]initWithFrame:CGRectMake(DefaultX, DefaultY, 240, 60)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    /// title label
    UILabel* lb = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 240, 30)];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.textColor = [UIColor redColor];
    lb.font = [UIFont systemFontOfSize:10.0];
    lb.text = @"Develop Version";
    [self addSubview:lb];
    
    /// choice segment
    UISegmentedControl * sg = [[UISegmentedControl alloc]initWithItems:@[@"线上环境",@"开发环境"]];
    sg.frame = CGRectMake(0,30, 240, 30);
    sg.selectedSegmentIndex = [MMDNetworkOption sharedMMDNetworkOption].currentNetworkEnvironmentSwitchOption;
    sg.tintColor = [UIColor redColor];
    [sg addTarget:self action:@selector(appServerChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:sg];
}

#pragma mark ------------ < Event > ------------
-(void)appServerChanged:(UISegmentedControl *)sender{
    
    DLOG_LINE(@"MMDSwitchEnvironmentView Had Been Changed >>> index = %ld",sender.selectedSegmentIndex);
    
    /// change
    [MMDNetworkOption sharedMMDNetworkOption].currentNetworkEnvironmentSwitchOption = sender.selectedSegmentIndex;
    
    /// save
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setObject:@(sender.selectedSegmentIndex) forKey:MMDNetworkEnvironmentSwitchOptionKey];
    [userDef synchronize];
}

@end
