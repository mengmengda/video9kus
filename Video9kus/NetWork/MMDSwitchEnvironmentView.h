//
//  MMDSwitchEnvironmentView.h
//  Video9kus
//
//  Created by Lincal on 2017/4/11.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDNetworkOption.h"

#define DefaultX 0
#define DefaultY 300

@interface MMDSwitchEnvironmentView : UIView

+(MMDSwitchEnvironmentView *)DefaultView;

@end
