//
//  ConfigHeader.h
//  
//
//  Created by Lincal on 15/11/13.
//  Copyright © 2015年 Lincal. All rights reserved.
//

#ifndef ConfigHeader_h
#define ConfigHeader_h


#endif /* ConfigHeader_h */

/// user
#define MMDUserUDID [FCUUID uuidForDevice]

/// app buy pid
#define MMDAPPPID @"AppStore"


//----------------------颜色类---------------------------
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//带有RGBA的颜色设置
#define COLOR(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]

// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)


#define UIDKey @"UID"
#define SetUID(uid) {\
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];\
    [userDefault setObject:uid forKey:UIDKey];[userDefault synchronize];\
}

#define UID [[NSUserDefaults standardUserDefaults] objectForKey:UIDKey]




#define TokenKey @"TOKEN"
#define SetToken(token) {\
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];\
    [userDefault setObject:token forKey:TokenKey];[userDefault synchronize];\
}

#define TOKEN [[NSUserDefaults standardUserDefaults] objectForKey:TokenKey]




#define LongtitudeKey @"longtitude"
#define SetLongtitude(Longtitude) {\
NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];\
[userDefault setObject:Longtitude forKey:LongtitudeKey];[userDefault synchronize];\
}

#define Longtitude [[NSUserDefaults standardUserDefaults] objectForKey:LongtitudeKey]






#define LatitudeKey @"latitude"
#define SetLatitude(Latitude) {\
NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];\
[userDefault setObject:Latitude forKey:LatitudeKey];[userDefault synchronize];\
}

#define Latitude [[NSUserDefaults standardUserDefaults] objectForKey:LatitudeKey]



#define setWeakSelf    __weak __typeof(self)weakSelf = self
#define setStrongSelf  __weak __typeof(self)weakSelf = self;__strong __typeof(weakSelf)strongSelf = weakSelf


//判断是否iOS8
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)



// 设备类型判断

#define ScreenW    ([[UIScreen mainScreen] bounds].size.width)
#define ScreenH    ([[UIScreen mainScreen] bounds].size.height)
#define ScreenMaxL (MAX(ScreenW, ScreenH))
#define ScreenMinL (MIN(ScreenW, ScreenH))
#define ScreenB    [[UIScreen mainScreen] bounds]

#define IsiPhone4   (IsiPhone && ScreenMaxL < 568.0)
#define IsiPhone5   (IsiPhone && ScreenMaxL == 568.0)
#define IsiPhone6   (IsiPhone && ScreenMaxL == 667.0)
#define IsiPhone6P  (IsiPhone && ScreenMaxL == 736.0)
#define IsiPad      (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IsiPhone    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IsRetain    ([[UIScreen mainScreen] scale] >= 2.0)

/// 版本号获取
#define MMDAppVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define MMDAppBuild [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]


#pragma mark - 测试用宏定义

#ifdef DEBUG
#define DLOG(...)     NSLog(@"%@",__VA_ARGS__);
#define DMMDLog(format, ...) NSLog(@"%s(%d): " format, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#define DLOG_METHOD   NSLog(@"%s", __func__);
#define DLOG_LINE(fmt, ...) NSLog((@"%s [Line %d]>> " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define TLOG111       NSLog(@"11111");
#define TLOG000       NSLog(@"00000");

#else
#define DLOG(...);
#define DMMDLog(format, ...);
#define DLOG_METHOD;
#define DLOG_LINE(fmt, ...);
#define TLOG111;
#define TLOG000;
#endif

