//
//  MMDRefresher.h
//  Video9kus
//
//  Created by Lincal on 2017/4/13.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^_Nullable MMDRefresherHeaderBlock) ();

@interface MMDRefresher : NSObject

/// default gif refresh header
+ (MJRefreshGifHeader *)defaultGifHeaderWithBlock:(MMDRefresherHeaderBlock)event;

@end

NS_ASSUME_NONNULL_END
