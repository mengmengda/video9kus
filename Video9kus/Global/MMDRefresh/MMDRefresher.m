//
//  MMDRefresher.m
//  Video9kus
//
//  Created by Lincal on 2017/4/13.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDRefresher.h"

const static int kImgsCount = 26;
const static int kGifH = 34;

@implementation MMDRefresher

+ (MJRefreshGifHeader *)defaultGifHeaderWithBlock:(MMDRefresherHeaderBlock)event{
    
    /// init
    MJRefreshGifHeader * gifH;
    if (event) {
        gifH = [MJRefreshGifHeader headerWithRefreshingBlock:event];
    }else{
        gifH = [[MJRefreshGifHeader alloc]init];
    }
    
    /// imgs
    NSMutableArray *imgs = [NSMutableArray array];
    for (int i = 1; i <= kImgsCount; i++) {
        NSString *imgName = @"s";
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%d", imgName, i]];
        [imgs addObject:image];
    }
    
    /// setting
    [gifH setImages:@[imgs[0]] forState:MJRefreshStateIdle];
    [gifH setImages:imgs forState:MJRefreshStateRefreshing];
    
    gifH.lastUpdatedTimeLabel.hidden = YES;
    gifH.stateLabel.hidden = NO;
    
    gifH.gifView.frame = CGRectMake(ScreenW / 2 - kGifH / 2, 5, kGifH , kGifH);
    gifH.gifView.contentMode = UIViewContentModeScaleAspectFill;
    
    gifH.stateLabel.frame = CGRectMake(ScreenW / 2 - 100 / 2, kGifH - 2 + 5, 100, 64 - kGifH);
    gifH.mj_h = 44 + 20;
    gifH.backgroundColor = [UIColor clearColor];
    
    gifH.stateLabel.font = [UIFont systemFontOfSize:11];
    [gifH setTitle:@"下拉更新" forState:MJRefreshStateIdle];
    [gifH setTitle:@"下拉更新" forState:MJRefreshStatePulling];
    [gifH setTitle:@"更新中..." forState:MJRefreshStateRefreshing];
    
    return gifH;
}

@end
