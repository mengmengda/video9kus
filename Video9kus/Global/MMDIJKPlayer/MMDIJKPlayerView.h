//
//  MMDIJKPlayerView.h
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IJKMediaFramework/IJKMediaFramework.h>

typedef NS_ENUM(NSInteger, MMDIJKPlayerStateType) {
    MMDIJKPlayerStateTypeStopped,
    MMDIJKPlayerStateTypePlaying,
    MMDIJKPlayerStateTypePaused,
    MMDIJKPlayerStateTypeInterrupted,
    MMDIJKPlayerStateTypeSeekingForward,
    MMDIJKPlayerStateTypeSeekingBackward
};

typedef void (^MMDIJKPlayerStateBlock) (MMDIJKPlayerStateType type);

@interface MMDIJKPlayerView : UIView

/// video player
@property (nonatomic,strong,readonly)id <IJKMediaPlayback> player;

/// video path
@property (nonatomic, strong ,readonly) NSURL * playURL;

/// video view
@property (nonatomic,strong, readonly) UIView * playerView;

/// state change Event Block
@property (nonatomic,strong) MMDIJKPlayerStateBlock MMDIJKPlayerStateEvent;

/// player is playing or not
@property (nonatomic,assign) BOOL playerIsPlaying;

/// init With url
- (void)loadPlayerForOnlineVideoWithURL:(NSURL *)url;

/// init player for native video
- (void)loadPlayerForNativeVideoWithPath:(NSURL *)pathURL;

/// play Video
- (void)startPlayVideo;

/// pause Video
- (void)pausePlayVideo;

/// jump to the point in time
- (void)jumpToTime:(NSTimeInterval)time;

/// fit new frame
- (void)fitFrameWith:(CGRect)frame;

/// adjust volume
- (void)setVolumeWithValue:(float)volumeValue;

@end
