//
//  MMDIJKPlayerView.m
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDIJKPlayerView.h"

@interface MMDIJKPlayerView ()

/// video player
@property (nonatomic,strong)id <IJKMediaPlayback> player;

/// video view
@property (nonatomic,strong) UIView * videoView;

@end

@implementation MMDIJKPlayerView

-(void)dealloc{
    
    [self removeVideoNotificationObservers];
    
    /// remove player
    if (_player) {
        [_player shutdown];
        [_player.view removeFromSuperview];
        _player = nil;
    }
    
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupVideoView];
        [self AddVideoNotificationObservers];
    }
    return self;
}

- (void)setupVideoView{
    
    /// init videoView
    self.videoView = [[UIView alloc]initWithFrame:self.bounds];
    [self addSubview:self.videoView];

}

#pragma mark ------------ < Notifiacation > ------------
- (void)AddVideoNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadStateDidChange:)
                                                 name:IJKMPMoviePlayerLoadStateDidChangeNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(VideoPlayBackFinish:)
                                                 name:IJKMPMoviePlayerPlaybackDidFinishNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                  object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoPlayBackStateDidChange:)
                                                 name:IJKMPMoviePlayerPlaybackStateDidChangeNotification
                                               object:_player];
    
}

- (void)removeVideoNotificationObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMoviePlayerLoadStateDidChangeNotification
                                                  object:_player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMoviePlayerPlaybackDidFinishNotification
                                                  object:_player];
 
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMoviePlayerPlaybackStateDidChangeNotification
                                                  object:_player];
    
}

#pragma mark ------------ < Open Method > ------------
- (void)loadPlayerForOnlineVideoWithURL:(NSURL *)url{
    [self loadPlayerWithURL:url Native:NO];
}

- (void)loadPlayerForNativeVideoWithPath:(NSURL *)pathURL{
    [self loadPlayerWithURL:pathURL Native:YES];
}

- (void)loadPlayerWithURL:(NSURL *)url Native:(BOOL)native{
    
    _playURL = url;
    
    /// init player
    if (native) {
        self.player = [[IJKAVMoviePlayerController alloc]initWithContentURL:url];
        self.player.playbackVolume = 0.5;
    }else{
        self.player = [[IJKFFMoviePlayerController alloc]initWithContentURL:url withOptions:nil];
        self.player.playbackVolume = 0.5;
    }
    self.player.shouldAutoplay = YES;
    
    /// relate
    _playerView = [self.player view];
    _playerView.frame = self.videoView.frame;
    _playerView.contentMode = UIViewContentModeScaleAspectFit;
    [self.videoView addSubview:_playerView];
    
    /// prepare
    [self.player prepareToPlay];
    
}

- (BOOL)playerIsPlaying{
    if (self.player.isPlaying) return YES;
    return NO;
}

- (void)startPlayVideo{
    [self.player play];
}

-(void)pausePlayVideo{
    [self.player pause];
}

- (void)jumpToTime:(NSTimeInterval)time{
    self.player.currentPlaybackTime = time;
}

-(void)fitFrameWith:(CGRect)frame{
    self.frame = frame;
    self.videoView.frame = self.bounds;
    self.playerView.frame = self.videoView.frame;
}

-(void)setVolumeWithValue:(float)volumeValue{
    self.player.playbackVolume = volumeValue;
}

#pragma mark ------------ < Event > ------------
- (void)loadStateDidChange:(NSNotification*)notification {
    IJKMPMovieLoadState loadState = _player.loadState;
    
    if ((loadState & IJKMPMovieLoadStatePlaythroughOK) != 0) {
        NSLog(@"Load State DidChange: IJKMovieLoadStatePlayThroughOK: %d\n",(int)loadState);
        
    }else if ((loadState & IJKMPMovieLoadStateStalled) != 0) {
        NSLog(@"load State DidChange: IJKMPMovieLoadStateStalled: %d\n", (int)loadState);
        
    } else {
        NSLog(@"load State DidChange: ???: %d\n", (int)loadState);
        
    }
}

- (void)mediaIsPreparedToPlayDidChange:(NSNotification*)notification {
    NSLog(@"mediaIsPrepareToPlayDidChange\n");
}

- (void)VideoPlayBackFinish:(NSNotification*)notification {
    int reason =[[[notification userInfo] valueForKey:IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    switch (reason) {
        case IJKMPMovieFinishReasonPlaybackEnded:
            NSLog(@"playback State DidChange: IJKMPMovieFinishReasonPlaybackEnded: %d\n", reason);
            if (self.MMDIJKPlayerStateEvent) self.MMDIJKPlayerStateEvent(MMDIJKPlayerStateTypeStopped);
            break;
            
        case IJKMPMovieFinishReasonUserExited:
            NSLog(@"playback State DidChange: IJKMPMovieFinishReasonUserExited: %d\n", reason);
            break;
            
        case IJKMPMovieFinishReasonPlaybackError:
            NSLog(@"playback State DidChange: IJKMPMovieFinishReasonPlaybackError: %d\n", reason);
            break;
            
        default:
            NSLog(@"playback State DidChange: ???: %d\n", reason);
            break;
    }
}

/// play state change
- (void)videoPlayBackStateDidChange:(NSNotification*)notification {
    switch (_player.playbackState) {
        case IJKMPMoviePlaybackStateStopped:
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: stoped", (int)_player.playbackState);
            break;
            
        case IJKMPMoviePlaybackStatePlaying:
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: playing", (int)_player.playbackState);
            if (self.MMDIJKPlayerStateEvent) self.MMDIJKPlayerStateEvent(MMDIJKPlayerStateTypePlaying);
            break;
            
        case IJKMPMoviePlaybackStatePaused:
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: paused", (int)_player.playbackState);
            if (self.MMDIJKPlayerStateEvent) self.MMDIJKPlayerStateEvent(MMDIJKPlayerStateTypePaused);
            break;
            
        case IJKMPMoviePlaybackStateInterrupted:
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: interrupted", (int)_player.playbackState);
            break;
            
        case IJKMPMoviePlaybackStateSeekingForward:{
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: seeking", (int)_player.playbackState);
            break;
        }
            
        case IJKMPMoviePlaybackStateSeekingBackward: {
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: seeking", (int)_player.playbackState);
            break;
        }
            
        default: {
            NSLog(@"MMDIJKPlayerView PlayBack State DidChange %d: unknown", (int)_player.playbackState);
            break;
        }
    }
}

@end
