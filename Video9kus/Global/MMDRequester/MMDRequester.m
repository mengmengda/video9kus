//
//  MMDRequester.m
//  Video9kus
//
//  Created by Lincal on 2017/4/14.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDRequester.h"
#import "MMDVideoTransformChecker.h"

@implementation MMDRequester

/// upload video at backgroud
+ (void)mmd_videoUploadWithDic:(NSDictionary *)parameters
                      success:(MMDRequesterSuccessBlock)success
                      failure:(MMDRequesterFailureBlock)failure{
    
    /// if id is null ,sign up again
    if (UserInfoModelEncryptID==nil || UserInfoModelEncryptID.length==0) [self mmd_visitorLoginWithDic:@{@"UUID":MMDUserUDID} success:nil failure:nil];
    
    NSData * f = [NSData dataWithContentsOfFile:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/video9kusTempMovie.mp4"]];
    
    [MMDNetworkManager mmd_UPLOAD:kURL(kVideoUpload) parameters:parameters fileData:f constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        
    } progress:^(NSProgress * _Nullable progress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
        
        ResponseModel * m;
        if (!responseObject) {
            [MBProgressHUD showError:@"nil"];
            return ;
        }else{
            m = [ResponseModel modelObjectWithDictionary:responseObject];
            if (m.status!=200)
            {
                if (m.errorMsg || m.errorMsg.length==0)
                {
                    [MBProgressHUD showError:m.errorMsg];
                    return;
                }
            }
        }
        
        if (success) success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD showError:[NSString stringWithFormat:@"%@",error.localizedDescription]];
        if (failure) failure(error);
        
    }];
    
}


+ (void)mmd_videoCheckWithDic:(NSDictionary *)parameters
                      success:(MMDRequesterSuccessBlock)success
                      failure:(MMDRequesterFailureBlock)failure{
    
    [MMDNetworkManager mmd_GET:kURL(kVideoCheck) parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
        
        ResponseModel * m;
        if (!responseObject) {
            [MBProgressHUD showError:@"nil"];
            return ;
        }else{
            m = [ResponseModel modelObjectWithDictionary:responseObject];
            if (m.status!=200)
            {
                if (m.errorMsg || m.errorMsg.length==0)
                {
//                    [MBProgressHUD showError:m.errorMsg];
//                    return;
                }
            }
        }
        
        if (success) success(m);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
}


+ (void)mmd_videoListWithDic:(NSDictionary *)parameters
                      success:(MMDRequesterSuccessBlock)success
                      failure:(MMDRequesterFailureBlock)failure{
 
    
    [MMDNetworkManager mmd_GET:kURL(kVideoList) parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
        
        ResponseModel * m;
        if (!responseObject) {
            [MBProgressHUD showError:@"nil"];
            return ;
        }else{
            m = [ResponseModel modelObjectWithDictionary:responseObject];
            if (m.status!=200)
            {
                if (m.errorMsg || m.errorMsg.length==0)
                {
//                    [MBProgressHUD showError:m.errorMsg];
                }
            }
        }
        
        if (success) success(m);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD showError:[NSString stringWithFormat:@"%@",error.localizedDescription]];
        if (failure) failure(error);
        
    }];

}

#pragma mark ------------ < Login > ------------
+ (void)mmd_loginWithDic:(NSDictionary *)parameters
                      success:(MMDRequesterSuccessBlock)success
                      failure:(MMDRequesterFailureBlock)failure{

    [MBProgressHUD showCustomLoadingWithMessage:@"登录中..." toView:nil];
    
    [MMDNetworkManager mmd_GET:kURL(kLogin) parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
        
        [MBProgressHUD hideHUD];
        
        NSLog(@"LINCALTEST !!!!!!!!!!!! %@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD hideHUD];
        NSLog(@"LINCALTEST ------------ %@",error.localizedDescription);
        
    }];

}

+ (void)mmd_visitorLoginWithDic:(NSDictionary *)parameters
                 success:(MMDRequesterSuccessBlock)success
                 failure:(MMDRequesterFailureBlock)failure{
    
    [MMDNetworkManager mmd_GET:kURL(kVisitorLogin) parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *  _Nullable responseObject) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUD];
        });
        
        ResponseModel * m;
        if (!responseObject) {
            [MBProgressHUD showError:@"nil"];
            return ;
        }else{
            m = [ResponseModel modelObjectWithDictionary:responseObject];
            if (m.status!=200)
            {
                if (m.errorMsg || m.errorMsg.length==0)
                {
                    [MBProgressHUD showError:m.errorMsg];
                    return;
                }
            }
        }
        
        UserInfoModel * um = [UserInfoModel modelObjectWithDictionary:[MMDNetworkManager jsonStrToContainers:m.content]];
        if (um.encryptId) [[NSUserDefaults standardUserDefaults]setObject:[MMDNetworkManager jsonStrToContainers:m.content] forKey:MMDUserInfoModelKey];
        if (success) success(um);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUD];
            [MBProgressHUD showError:[NSString stringWithFormat:@"%@",error.localizedDescription]];
        });
        if (failure) failure(error);
        
    }];

}

@end
