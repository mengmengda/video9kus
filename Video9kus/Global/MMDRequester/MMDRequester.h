//
//  MMDRequester.h
//  Video9kus
//
//  Created by Lincal on 2017/4/14.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// return content is inconclusive
typedef void (^_Nullable MMDRequesterSuccessBlock) (id _Nonnull res);

typedef void (^_Nullable MMDRequesterFailureBlock) (NSError * _Nonnull err);

@interface MMDRequester : NSObject

/// upload video at backgroud
+ (void)mmd_videoUploadWithDic:(NSDictionary *)parameters
                       success:(MMDRequesterSuccessBlock)success
                       failure:(MMDRequesterFailureBlock)failure;

/// check video transform result
+ (void)mmd_videoCheckWithDic:(NSDictionary *)parameters
                      success:(MMDRequesterSuccessBlock)success
                      failure:(MMDRequesterFailureBlock)failure;

/// video list
+ (void)mmd_videoListWithDic:(NSDictionary *)parameters
                     success:(MMDRequesterSuccessBlock)success
                     failure:(MMDRequesterFailureBlock)failure;

/// sign in
+ (void)mmd_loginWithDic:(NSDictionary *)parameters
                 success:(MMDRequesterSuccessBlock)success
                 failure:(MMDRequesterFailureBlock)failure;

/// sign up
+ (void)mmd_visitorLoginWithDic:(NSDictionary *)parameters
                        success:(MMDRequesterSuccessBlock)success
                        failure:(MMDRequesterFailureBlock)failure;

@end

NS_ASSUME_NONNULL_END
