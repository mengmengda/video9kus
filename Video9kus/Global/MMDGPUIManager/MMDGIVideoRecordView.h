//
//  MMDGIVideoRecordView.h
//  Video9kus
//
//  Created by Lincal on 2017/4/5.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"

/// Max Record Time Length
#define kMMDGIVideoRecordMaxRT 5.0f
/// Refresh Interval
#define kMMDGIVideoRecordRefreshInterval 0.05f
/// temp Path
#define kMMDGIVideotempPath [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/video9kusTempMovie.mp4"]

typedef void(^MMDGIVideoRecordCompletedBlock)();

typedef void(^MMDGIVideoRecordMaxTimeBlock)();

typedef NS_ENUM(NSInteger, MMDGICaptureDevicePosition) {
    MMDGICaptureDevicePositionBack,
    MMDGICaptureDevicePositionFront,
};

@interface MMDGIVideoRecordView : UIView

/// device position. Default is back
@property (nonatomic,assign) MMDGICaptureDevicePosition position;

/// have recorded time length
@property (nonatomic,assign,readonly) float recordedTime;

/// call this Block after timeout
@property (nonatomic,strong) MMDGIVideoRecordMaxTimeBlock MMDGIVideoRecordMaxTimeEvent;

/// call this Block after record completed
@property (nonatomic,strong) MMDGIVideoRecordCompletedBlock MMDGIVideoRecordCompletedEvent;

/// know that the recorder is recording or not
@property (nonatomic,assign,readonly) BOOL isRecording;

/// start video record
- (void)startVideoRecord;

/// stop video record
- (void)stopVideoRecord;

/// start session
- (void)startVideoSession;

/// stop session
- (void)stopVideoSeesion;

/// focus
- (void)focusWithPoint:(CGPoint)point;

@end
