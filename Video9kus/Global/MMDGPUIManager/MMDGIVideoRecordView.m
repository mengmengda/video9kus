//
//  MMDGIVideoRecordView.m
//  Video9kus
//
//  Created by Lincal on 2017/4/5.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDGIVideoRecordView.h"

@interface MMDGIVideoRecordView ()<GPUImageMovieWriterDelegate>

/// Record
@property (nonatomic,strong) GPUImageVideoCamera * videoCamera;
@property (nonatomic,strong) GPUImageSaturationFilter * saturationF;
@property (nonatomic,strong) GPUImageView * videoV;

/// Write
@property (nonatomic,copy) NSString * moviePath;
@property (nonatomic,strong) GPUImageMovieWriter * movieWriter;

/// Queue
@property (nonatomic,strong) dispatch_queue_t videoQueue;

/// Timer
@property (nonatomic,strong) NSTimer * recordT;
@property (nonatomic,assign,readwrite) float recordedTime;

/// status
@property (nonatomic,assign,readwrite) BOOL isRecording;

@end


@implementation MMDGIVideoRecordView

-(void)dealloc{
    if (self.recordT) {
        [self.recordT invalidate];
        self.recordT = nil;
    }
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor lightGrayColor];
        
        /// time
        self.recordedTime = 0;
        
        /// init VideoRecorder
        self.videoCamera = [[GPUImageVideoCamera alloc]initWithSessionPreset:AVCaptureSessionPreset1280x720 cameraPosition:AVCaptureDevicePositionBack];
        self.videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        [self.videoCamera addAudioInputsAndOutputs];

        /// init Filter
        self.saturationF = [[GPUImageSaturationFilter alloc]init];
        
        /// init FilterVideoView
        self.videoV = [[GPUImageView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        
        /// Add Target
        [self.videoCamera addTarget:self.saturationF];
        [self.saturationF addTarget:self.videoV];
        
        /// Add UI
        [self addSubview:self.videoV];
        
        /// Start Session
        [self.videoCamera startCameraCapture];
        
    }
    return self;
}

#pragma mark ------------ < Interior Method > ------------
- (CGPoint)captureDevicePointForPoint:(CGPoint)point {
    /// size should not be different from touch target size
    if (self.videoCamera.cameraPosition == AVCaptureDevicePositionBack){
        point = CGPointMake( point.y / ScreenH ,1 - point.x / ScreenW);
    }else{
        point = CGPointMake(point.y / ScreenH ,point.x / ScreenW);
    }
    return point;
}

#pragma mark ------------ < Lazy Load > ------------
- (dispatch_queue_t)videoQueue{
    if (!_videoQueue) {
        _videoQueue = dispatch_queue_create("com.MMD.VideoQueue", NULL);
    }
    return _videoQueue;
}

#pragma mark ------------ < Open Method > ------------
- (void)startVideoRecord{
    
    /// Prepare Save URL
    self.moviePath = kMMDGIVideotempPath;
    unlink([self.moviePath UTF8String]);
    NSURL * moviePathURL = [NSURL fileURLWithPath:self.moviePath];
   
    /// init Writer
    self.movieWriter = [[GPUImageMovieWriter alloc]initWithMovieURL:moviePathURL size:CGSizeMake(720.0, 1280.0)];
    self.movieWriter.encodingLiveVideo = YES;
    self.movieWriter.shouldPassthroughAudio = NO;
    self.movieWriter.delegate = self;

    /// Add Target
    [self.saturationF addTarget:self.movieWriter];
    self.videoCamera.audioEncodingTarget = self.movieWriter;
    
    /// Start Record
    [self.movieWriter startRecording];
    
    /// Timer
    self.recordT = [NSTimer scheduledTimerWithTimeInterval:kMMDGIVideoRecordRefreshInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
        self.recordedTime = self.recordedTime + kMMDGIVideoRecordRefreshInterval;
        // timeout
        if (self.recordedTime>=kMMDGIVideoRecordMaxRT) {
            [self stopVideoRecord];
            if (self.MMDGIVideoRecordMaxTimeEvent) self.MMDGIVideoRecordMaxTimeEvent();
        }
    }];
    
    /// change status
    _isRecording = YES;
    
}

- (void)stopVideoRecord{
    
    /// Stop Record
    [self.movieWriter finishRecording];
    
    /// Cancle Relevance
    self.videoCamera.audioEncodingTarget = nil;
    
    /// Save To Album
    UISaveVideoAtPathToSavedPhotosAlbum(self.moviePath, nil, nil, nil);
    
    /// Remove Target
    [self.saturationF removeTarget:self.movieWriter];
    
    /// Timer
    [self.recordT invalidate];
    self.recordT = nil;
    self.recordedTime = 0;
}

-(void)startVideoSession{
    if (!self.videoCamera.isRunning) {
        dispatch_async(self.videoQueue, ^{
            [self.videoCamera startCameraCapture];
        });
    }
}

- (void)stopVideoSeesion{
    if (self.videoCamera.isRunning) {
        dispatch_async(self.videoQueue, ^{
            [self.videoCamera stopCameraCapture];
        });
    }
}

- (void)setPosition:(MMDGICaptureDevicePosition)position {
    _position = position;
    switch (_position) {
        case MMDGICaptureDevicePositionBack: {
            if (self.videoCamera.cameraPosition != AVCaptureDevicePositionBack) {
                [self.videoCamera pauseCameraCapture];
                dispatch_async(self.videoQueue, ^{
                    [self.videoCamera rotateCamera];
                    [self.videoCamera resumeCameraCapture];
                });
            }
        }
            break;
        case MMDGICaptureDevicePositionFront: {
            if (self.videoCamera.cameraPosition != AVCaptureDevicePositionFront) {
                [self.videoCamera pauseCameraCapture];
                dispatch_async(self.videoQueue, ^{
                    self.videoCamera.horizontallyMirrorFrontFacingCamera = YES;
                    [self.videoCamera rotateCamera];
                    [self.videoCamera resumeCameraCapture];
                });
            }
        }
            break;
        default:
            break;
    }
}

static const NSString * MMDVideoCameraAdjustingExposureContext;

- (void)focusWithPoint:(CGPoint)point{
    
    CGPoint p = [self captureDevicePointForPoint:point];
    
    AVCaptureDevice * device = self.videoCamera.inputCamera;
    
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError * err;
        
        /// set focus point
        if ([device lockForConfiguration:&err])
        {
            [device setFocusPointOfInterest:p];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            [device unlockForConfiguration];
        } else {
            DMMDLog(@"Focus Setting Lock Error = %@", err);
        }
    }
    
    /// call exposure method
    [self exposureWithDevicePoint:p];
    
}

- (void)exposureWithDevicePoint:(CGPoint)point{
    
    AVCaptureDevice * device = self.videoCamera.inputCamera;
    
    /// set exposure point
    if([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]){
        NSError * err;
        
        /// set focus point
        if ([device lockForConfiguration:&err])
        {
            [device setExposurePointOfInterest:point];
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            if ([device isExposureModeSupported:AVCaptureExposureModeLocked]) {
                [device addObserver:self
                         forKeyPath:@"adjustingExposure"
                            options:NSKeyValueObservingOptionNew
                            context:&MMDVideoCameraAdjustingExposureContext];
            }
            [device unlockForConfiguration];
        } else {
            DMMDLog(@"Exposure Setting Lock Error = %@", err);
        }
    }
    
}

#pragma mark ------------ < Delegate > ------------
-(void)movieRecordingCompleted{
    DLOG(@"Video Record Completed");
    /// change status
    _isRecording = NO;
    /// call back
    if (self.MMDGIVideoRecordCompletedEvent) self.MMDGIVideoRecordCompletedEvent();
}

- (void)movieRecordingFailedWithError:(NSError*)error{
    DLOG(@"Video Record Failed");
}

#pragma mark ------------ < Event > ------------
-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                      context:(void *)context{
    
    if (context == &MMDVideoCameraAdjustingExposureContext)
    {
        
        AVCaptureDevice * d = (AVCaptureDevice *)object;
        
        if (!d.isAdjustingExposure && [d isExposureModeSupported:AVCaptureExposureModeLocked])
        {
            /// remove observer
            [object removeObserver:self
                        forKeyPath:@"adjustingExposure"
                           context:&MMDVideoCameraAdjustingExposureContext];
            
            /// change mode
            dispatch_async(dispatch_get_main_queue(), ^
            {
                NSError * err;
                if ([d lockForConfiguration:&err])
                {
                    [d setExposureMode:AVCaptureExposureModeLocked];
                    [d unlockForConfiguration];
                }else{
                    DMMDLog(@"Exposure Setting Lock Error = %@", err);
                }
            });
        }
        
    }else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
    
}

@end
