//
//  MMDCircleProgressView.h
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMDCircleProgressView : UIView

-(void)updateProgressWithValue:(CGFloat)progress;

-(void)resetProgress;

@end
