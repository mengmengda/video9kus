//
//  MMDVideoToolView.m
//  Video9kus
//
//  Created by Lincal on 17/3/30.
//  Copyright (c) 2017年 MMD. All rights reserved.
//

#import "MMDVideoToolView.h"

@interface MMDVideoToolView ()

@property (weak, nonatomic) IBOutlet UIButton *topFirstBtn;

@property (weak, nonatomic) IBOutlet UIButton *topSecendBtn;

@end


@implementation MMDVideoToolView

-(void)dealloc{
    [self.bottomMiddleBtn removeObserver:self forKeyPath:@"selected"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    /// register event
    [self.bottomMiddleBtn addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    
}

#pragma mark ------------ < Open Method > ------------
+(instancetype)DefaultView{
    MMDVideoToolView * v = [[[NSBundle mainBundle] loadNibNamed:@"MMDVideoToolView" owner:nil options:nil]lastObject];
    v.frame = CGRectMake(0, 0, ScreenW, ScreenH);
    return v;
}

#pragma mark ------------ < Event > ------------
- (IBAction)BtnClick:(UIButton *)sender {
    
    /// Change Status
    if (sender.selected) {
        sender.selected = NO;
    }else{
        sender.selected = YES;
    }
    
    /// Change UI
    switch (sender.tag) {
        case MMDBtnTypeBottomMiddleBtn:
            break;
        default:
            break;
    }
    
    /// Event
    if (self.MMDVideoToolViewBtnEvent) {
        self.MMDVideoToolViewBtnEvent(sender.tag, sender);
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context{
    
    if ([keyPath isEqualToString:@"selected"]) {
        
        BOOL b = [(NSNumber *)[change objectForKey:@"new"]boolValue];;
        if (b) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.bottomMiddleBtn setTitle:@"00:00" forState:UIControlStateNormal];
                self.bottomMiddleBtn.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
            }];
        }else{
            [UIView animateWithDuration:0.5 animations:^{
                [self.bottomMiddleBtn setTitle:@"" forState:UIControlStateNormal];
                self.bottomMiddleBtn.backgroundColor = UIColorFromRGB(0xfdda35);
            }];
        }
    }
}

@end
