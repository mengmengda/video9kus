//
//  MMDVideoToolView.h
//  Video9kus
//
//  Created by Lincal on 17/3/30.
//  Copyright (c) 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDCircleProgressView.h"

typedef NS_ENUM (NSInteger ,MMDVideoToolViewBtnType){
    MMDBtnTypeTopFirstBtn = 200,
    MMDBtnTypeTopSecendBtn,
    MMDBtnTypeBottomMiddleBtn,
};

typedef void(^MMDVideoToolViewBtnBlock)(MMDVideoToolViewBtnType type,UIButton * btn);

@interface MMDVideoToolView : UIView

@property (nonatomic,strong)MMDVideoToolViewBtnBlock MMDVideoToolViewBtnEvent;

@property (weak, nonatomic) IBOutlet UIButton *bottomMiddleBtn;

@property (weak, nonatomic) IBOutlet MMDCircleProgressView *progressV;

+(instancetype)DefaultView;

@end

