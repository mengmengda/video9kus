//
//  MMDDocumentManager.m
//  Video9kus
//
//  Created by Lincal on 2017/4/13.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDDocumentManager.h"

@implementation MMDDocumentManager

#pragma mark ------------ < File Check > ------------
+ (BOOL)fileExistAtPath:(NSString *)path{
    NSFileManager * fM = [NSFileManager defaultManager];
    if ([fM fileExistsAtPath:path isDirectory:NULL]) return YES;
    return NO;
}

#pragma mark ------------ < Folder Create > ------------
+(BOOL)createFileDir{
    NSFileManager * fM = [[NSFileManager alloc] init];
    NSString * path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * targetPath = [NSString stringWithFormat:@"%@%@", path,videoFolder];
    
    /// create after judge
    if (![[NSFileManager defaultManager] fileExistsAtPath:targetPath]) {
        [fM createDirectoryAtPath:targetPath withIntermediateDirectories:YES attributes:nil error:nil];
        return YES;
    } else {
        NSLog(@"FileDir is exists.");
    }
    return NO;
}

#pragma mark ------------ < Path Create > ------------
+ (NSString *)documentPath{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsPath = paths[0];
    if (!documentsPath) {
        NSLog(@"Documents is not existe");
    }
    return documentsPath;
}

+ (NSString *)createFilePathWithFileName:(NSString *)fileName{
    NSString *documentsPath = [self documentPath];
    if (![self fileExistAtPath:[documentsPath stringByAppendingPathComponent:videoFolder]]) [self createFileDir];
    if (documentsPath) return [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",videoFolder,fileName]];
    return nil;
}

#pragma mark ------------ < File Handle > ------------
+(NSString *)saveVideoWithFileName:(NSString *)fileName VideoData:(NSData *)data{
    
    NSString * uniquePath = [self createFilePathWithFileName:fileName];

    /// check
    BOOL cr = [[NSFileManager defaultManager]fileExistsAtPath:uniquePath];
    if(cr){
        DLOG(@"Already Have .Save Fail");
        return nil;
    }
    
    /// save
    BOOL sr = [data writeToFile:uniquePath atomically:YES];
    if(!sr){
        DLOG(@"imageJPEG Save no success");
        return nil;
    }
    
    return uniquePath;
}

+ (BOOL)removeFileAtPath:(NSString *)filePath{
    NSError *error = nil;
    if ([self fileExistAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (error) {
            NSLog(@"Failure the error info ：%@", error);
            return NO;
        }
        else {
            NSLog(@"Success had removed the file");
            return YES;
        }
    }
    else {
        NSLog(@"Failure file is not exist");
    }
    return NO;
}

@end
