//
//  MMDDocumentManager.h
//  Video9kus
//
//  Created by Lincal on 2017/4/13.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

/// download video save in here
#define videoFolder @"/userDownloadVideo"

@interface MMDDocumentManager : NSObject

/// judge the file exist or not
+ (BOOL)fileExistAtPath:(NSString *)path;

/// create file at path which setup before
+(BOOL)createFileDir;

/// get the document path
+ (NSString *)documentPath;

/// get the file form custom document path
+ (NSString *)createFilePathWithFileName:(NSString *)fileName;

/// write video data to file
+ (NSString *)saveVideoWithFileName:(NSString *)fileName VideoData:(NSData *)data;

/// remove file
+ (BOOL)removeFileAtPath:(NSString *)filePath;

@end
