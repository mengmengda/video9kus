//
//  MMDVideoTransformChecker.h
//  Video9kus
//
//  Created by Lincal on 2017/4/14.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

/// check time interval
#define MMDVideoTransformCheckerInterval 30.0f

/// notification name
#define MMDVideoTransformCheckerNotiName @"MMDMMDVideoTransformCheckerNotiName"

@interface MMDVideoTransformChecker : NSObject

singleton_interface(MMDVideoTransformChecker);

- (void)startChecker;

@end
