//
//  MMDVideoTransformChecker.m
//  Video9kus
//
//  Created by Lincal on 2017/4/14.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MMDVideoTransformChecker.h"

@interface MMDVideoTransformChecker ()

@property (nonatomic,strong)NSTimer * timer;

@property (nonatomic,strong) NSMutableArray * mArr;

@end

@implementation MMDVideoTransformChecker

singleton_implementation(MMDVideoTransformChecker);

- (void)dealloc{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)startChecker{
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:MMDVideoTransformCheckerInterval target:self selector:@selector(checkAtRegularTime) userInfo:nil repeats:YES];
    
}

- (void)checkAtRegularTime{
    
    NSDictionary * p = @{
                         @"encryptId":UserInfoModelEncryptID,
                         };
    
    setWeakSelf;
    [MMDRequester mmd_videoCheckWithDic:p success:^(ResponseModel * _Nonnull res) {
        
        NSArray * arr = [MMDNetworkManager jsonStrToContainers:res.content];
        [arr enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            VideoCheckModel * m  = [VideoCheckModel modelObjectWithDictionary:obj];
            DMMDLog(@"LINCALTEST Checking Result %lf , VideoID %@",m.transformStatus,m.videoId);
            
            // notification
            if (m.transformStatus==1 || m.transformStatus==2) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:MMDVideoTransformCheckerNotiName object:nil];
                });
            }
            
        }];
        
        if (arr.count==0 || arr==nil) {
            [weakSelf.timer invalidate];
            weakSelf.timer = nil;
        }
        
    } failure:nil];
    
}

@end
