//
//  UserInfoModel.m
//
//  Created by  Lincal on 2017/4/13
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import "UserInfoModel.h"
#import "HomeViewController.h"

NSString *const kUserInfoModelUserName = @"userName";
NSString *const kUserInfoModelNickName = @"nickName";
NSString *const kUserInfoModelEncryptId = @"encryptId";
NSString *const kUserInfoModelRegisterType = @"registerType";
NSString *const kUserInfoModelAvatar = @"avatar";


@interface UserInfoModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation UserInfoModel

singleton_implementation(UserInfoModel);

@synthesize userName = _userName;
@synthesize nickName = _nickName;
@synthesize encryptId = _encryptId;
@synthesize registerType = _registerType;
@synthesize avatar = _avatar;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userName = [self objectOrNilForKey:kUserInfoModelUserName fromDictionary:dict];
            self.nickName = [self objectOrNilForKey:kUserInfoModelNickName fromDictionary:dict];
            self.encryptId = [self objectOrNilForKey:kUserInfoModelEncryptId fromDictionary:dict];
            self.registerType = [[self objectOrNilForKey:kUserInfoModelRegisterType fromDictionary:dict] doubleValue];
            self.avatar = [self objectOrNilForKey:kUserInfoModelAvatar fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userName forKey:kUserInfoModelUserName];
    [mutableDict setValue:self.nickName forKey:kUserInfoModelNickName];
    [mutableDict setValue:self.encryptId forKey:kUserInfoModelEncryptId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.registerType] forKey:kUserInfoModelRegisterType];
    [mutableDict setValue:self.avatar forKey:kUserInfoModelAvatar];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.userName = [aDecoder decodeObjectForKey:kUserInfoModelUserName];
    self.nickName = [aDecoder decodeObjectForKey:kUserInfoModelNickName];
    self.encryptId = [aDecoder decodeObjectForKey:kUserInfoModelEncryptId];
    self.registerType = [aDecoder decodeDoubleForKey:kUserInfoModelRegisterType];
    self.avatar = [aDecoder decodeObjectForKey:kUserInfoModelAvatar];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userName forKey:kUserInfoModelUserName];
    [aCoder encodeObject:_nickName forKey:kUserInfoModelNickName];
    [aCoder encodeObject:_encryptId forKey:kUserInfoModelEncryptId];
    [aCoder encodeDouble:_registerType forKey:kUserInfoModelRegisterType];
    [aCoder encodeObject:_avatar forKey:kUserInfoModelAvatar];
}

- (id)copyWithZone:(NSZone *)zone {
    UserInfoModel *copy = [[UserInfoModel alloc] init];
    
    
    
    if (copy) {

        copy.userName = [self.userName copyWithZone:zone];
        copy.nickName = [self.nickName copyWithZone:zone];
        copy.encryptId = [self.encryptId copyWithZone:zone];
        copy.registerType = self.registerType;
        copy.avatar = [self.avatar copyWithZone:zone];
    }
    
    return copy;
}

-(double)registerType{
    if (!_registerType) {
        NSDictionary * d = [self getDic];
        NSNumber * n = (NSNumber *)d[@"registerType"];
        if (d) {
            _registerType = [n doubleValue];
            return _registerType;
        }
        DLOG(@"warning. registerType is nil . will return 0");
        return 0;
    }
    return _registerType;
}

-(NSString *)avatar{
    if (!_avatar) {
        NSDictionary * d = [self getDic];
        NSString * s = d[@"avatar"];
        if (d && s && s.length!=0) {
            _avatar = s;
            return _avatar;
        }
        DLOG(@"warning. avatar is nil . will return empty string");
        return @"";
    }
    return _avatar;
}

-(NSString *)nickName{
    if (!_nickName) {
        NSDictionary * d = [self getDic];
        NSString * s = d[@"nickName"];
        if (d && s && s.length!=0) {
            _nickName = s;
            return _nickName;
        }
        DLOG(@"warning. nickName is nil . will return empty string");
        return @"";
    }
    return _nickName;
}

- (NSString *)userName{
    if (!_userName) {
        NSDictionary * d = [self getDic];
        NSString * s = d[@"userName"];
        if (d && s && s.length!=0) {
            _userName = s;
            return _userName;
        }
        DLOG(@"warning. userName is nil . will return empty string");
        return @"";
    }
    return _userName;
}

-(NSString *)encryptId{
    if (!_encryptId) {
        NSDictionary * d = [self getDic];
        NSString * s = d[@"encryptId"];
        if (d && s && s.length!=0) {
            _encryptId = s;
            return _encryptId;
        }
        [MBProgressHUD showError:@"ID为空"];
        DLOG(@"warning. ID is nil . will return empty string");
        return @"";
    }
    return _encryptId;
}

-(NSDictionary *)getDic{
    NSDictionary * d = [[NSUserDefaults standardUserDefaults]objectForKey:MMDUserInfoModelKey];
    if (d) return d;
    return nil;
}

#pragma mark ------------ < Open Method > ------------
- (void)removeDic{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:MMDUserInfoModelKey];
}

@end
