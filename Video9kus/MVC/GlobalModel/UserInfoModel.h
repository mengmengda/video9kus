//
//  UserInfoModel.h
//
//  Created by  Lincal on 2017/4/13
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UserInfoModelNickName [UserInfoModel sharedUserInfoModel].nickName
#define UserInfoModelEncryptID [UserInfoModel sharedUserInfoModel].encryptId
#define UserInfoModelAvatar [UserInfoModel sharedUserInfoModel].avatar

#define MMDUserInfoModelKey @"MMDUserInfoModelKey"

@interface UserInfoModel : NSObject <NSCoding, NSCopying>

singleton_interface(UserInfoModel);

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *encryptId;
@property (nonatomic, assign) double registerType;
@property (nonatomic, strong) NSString *avatar;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

/// remove the saved dic
- (void)removeDic;

@end
