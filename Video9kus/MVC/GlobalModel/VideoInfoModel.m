//
//  VideoInfoModel.m
//
//  Created by  Lincal on 2017/4/17
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import "VideoInfoModel.h"


NSString *const kVideoInfoModelVTime = @"v_time";
NSString *const kVideoInfoModelDetail = @"detail";
NSString *const kVideoInfoModelStyle = @"style";
NSString *const kVideoInfoModelVideoImg = @"video_img";
NSString *const kVideoInfoModelTransformStatus = @"transform_status";
NSString *const kVideoInfoModelTitle = @"title";
NSString *const kVideoInfoModelVideoId = @"video_id";
NSString *const kVideoInfoModelVideoPath = @"video_path";
NSString *const kVideoInfoModelType = @"type";
NSString *const kVideoInfoModelTags = @"tags";
NSString *const kVideoInfoModelAddTime = @"add_time";


@interface VideoInfoModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation VideoInfoModel

@synthesize vTime = _vTime;
@synthesize detail = _detail;
@synthesize style = _style;
@synthesize videoImg = _videoImg;
@synthesize transformStatus = _transformStatus;
@synthesize title = _title;
@synthesize videoId = _videoId;
@synthesize videoPath = _videoPath;
@synthesize type = _type;
@synthesize tags = _tags;
@synthesize addTime = _addTime;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.vTime = [self objectOrNilForKey:kVideoInfoModelVTime fromDictionary:dict];
            self.detail = [self objectOrNilForKey:kVideoInfoModelDetail fromDictionary:dict];
            self.style = [self objectOrNilForKey:kVideoInfoModelStyle fromDictionary:dict];
            self.videoImg = [self objectOrNilForKey:kVideoInfoModelVideoImg fromDictionary:dict];
            self.transformStatus = [[self objectOrNilForKey:kVideoInfoModelTransformStatus fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kVideoInfoModelTitle fromDictionary:dict];
            self.videoId = [[self objectOrNilForKey:kVideoInfoModelVideoId fromDictionary:dict] doubleValue];
            self.videoPath = [self objectOrNilForKey:kVideoInfoModelVideoPath fromDictionary:dict];
            self.type = [[self objectOrNilForKey:kVideoInfoModelType fromDictionary:dict] doubleValue];
            self.tags = [self objectOrNilForKey:kVideoInfoModelTags fromDictionary:dict];
            self.addTime = [[self objectOrNilForKey:kVideoInfoModelAddTime fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.vTime forKey:kVideoInfoModelVTime];
    [mutableDict setValue:self.detail forKey:kVideoInfoModelDetail];
    [mutableDict setValue:self.style forKey:kVideoInfoModelStyle];
    [mutableDict setValue:self.videoImg forKey:kVideoInfoModelVideoImg];
    [mutableDict setValue:[NSNumber numberWithDouble:self.transformStatus] forKey:kVideoInfoModelTransformStatus];
    [mutableDict setValue:self.title forKey:kVideoInfoModelTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.videoId] forKey:kVideoInfoModelVideoId];
    [mutableDict setValue:self.videoPath forKey:kVideoInfoModelVideoPath];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kVideoInfoModelType];
    [mutableDict setValue:self.tags forKey:kVideoInfoModelTags];
    [mutableDict setValue:[NSNumber numberWithDouble:self.addTime] forKey:kVideoInfoModelAddTime];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.vTime = [aDecoder decodeObjectForKey:kVideoInfoModelVTime];
    self.detail = [aDecoder decodeObjectForKey:kVideoInfoModelDetail];
    self.style = [aDecoder decodeObjectForKey:kVideoInfoModelStyle];
    self.videoImg = [aDecoder decodeObjectForKey:kVideoInfoModelVideoImg];
    self.transformStatus = [aDecoder decodeDoubleForKey:kVideoInfoModelTransformStatus];
    self.title = [aDecoder decodeObjectForKey:kVideoInfoModelTitle];
    self.videoId = [aDecoder decodeDoubleForKey:kVideoInfoModelVideoId];
    self.videoPath = [aDecoder decodeObjectForKey:kVideoInfoModelVideoPath];
    self.type = [aDecoder decodeDoubleForKey:kVideoInfoModelType];
    self.tags = [aDecoder decodeObjectForKey:kVideoInfoModelTags];
    self.addTime = [aDecoder decodeDoubleForKey:kVideoInfoModelAddTime];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_vTime forKey:kVideoInfoModelVTime];
    [aCoder encodeObject:_detail forKey:kVideoInfoModelDetail];
    [aCoder encodeObject:_style forKey:kVideoInfoModelStyle];
    [aCoder encodeObject:_videoImg forKey:kVideoInfoModelVideoImg];
    [aCoder encodeDouble:_transformStatus forKey:kVideoInfoModelTransformStatus];
    [aCoder encodeObject:_title forKey:kVideoInfoModelTitle];
    [aCoder encodeDouble:_videoId forKey:kVideoInfoModelVideoId];
    [aCoder encodeObject:_videoPath forKey:kVideoInfoModelVideoPath];
    [aCoder encodeDouble:_type forKey:kVideoInfoModelType];
    [aCoder encodeObject:_tags forKey:kVideoInfoModelTags];
    [aCoder encodeDouble:_addTime forKey:kVideoInfoModelAddTime];
}

- (id)copyWithZone:(NSZone *)zone {
    VideoInfoModel *copy = [[VideoInfoModel alloc] init];
    
    
    
    if (copy) {

        copy.vTime = [self.vTime copyWithZone:zone];
        copy.detail = [self.detail copyWithZone:zone];
        copy.style = [self.style copyWithZone:zone];
        copy.videoImg = [self.videoImg copyWithZone:zone];
        copy.transformStatus = self.transformStatus;
        copy.title = [self.title copyWithZone:zone];
        copy.videoId = self.videoId;
        copy.videoPath = [self.videoPath copyWithZone:zone];
        copy.type = self.type;
        copy.tags = [self.tags copyWithZone:zone];
        copy.addTime = self.addTime;
    }
    
    return copy;
}


@end
