//
//  VideoCheckModel.h
//
//  Created by  Lincal on 2017/4/17
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface VideoCheckModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double transformStatus;
@property (nonatomic, strong) NSString *videoId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
