//
//  VideoInfoModel.h
//
//  Created by  Lincal on 2017/4/17
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface VideoInfoModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *vTime;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, strong) NSString *style;
@property (nonatomic, strong) NSString *videoImg;
@property (nonatomic, assign) double transformStatus;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double videoId;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, assign) double type;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, assign) double addTime;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
