//
//  VideoCheckModel.m
//
//  Created by  Lincal on 2017/4/17
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import "VideoCheckModel.h"


NSString *const kVideoCheckModelTransformStatus = @"transform_status";
NSString *const kVideoCheckModelVideoId = @"video_id";


@interface VideoCheckModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation VideoCheckModel

@synthesize transformStatus = _transformStatus;
@synthesize videoId = _videoId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.transformStatus = [[self objectOrNilForKey:kVideoCheckModelTransformStatus fromDictionary:dict] doubleValue];
            self.videoId = [self objectOrNilForKey:kVideoCheckModelVideoId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.transformStatus] forKey:kVideoCheckModelTransformStatus];
    [mutableDict setValue:self.videoId forKey:kVideoCheckModelVideoId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.transformStatus = [aDecoder decodeDoubleForKey:kVideoCheckModelTransformStatus];
    self.videoId = [aDecoder decodeObjectForKey:kVideoCheckModelVideoId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_transformStatus forKey:kVideoCheckModelTransformStatus];
    [aCoder encodeObject:_videoId forKey:kVideoCheckModelVideoId];
}

- (id)copyWithZone:(NSZone *)zone {
    VideoCheckModel *copy = [[VideoCheckModel alloc] init];
    
    
    
    if (copy) {

        copy.transformStatus = self.transformStatus;
        copy.videoId = [self.videoId copyWithZone:zone];
    }
    
    return copy;
}


@end
