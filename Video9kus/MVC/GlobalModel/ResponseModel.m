//
//  ResponseModel.m
//
//  Created by  Lincal on 2017/4/17
//  Copyright (c) 2017 Lincal. All rights reserved.
//

#import "ResponseModel.h"


NSString *const kResponseModelContent = @"content";
NSString *const kResponseModelStatus = @"status";
NSString *const kResponseModelErrorMsg = @"errorMsg";


@interface ResponseModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ResponseModel

@synthesize content = _content;
@synthesize status = _status;
@synthesize errorMsg = _errorMsg;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.content = [self objectOrNilForKey:kResponseModelContent fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kResponseModelStatus fromDictionary:dict] doubleValue];
            self.errorMsg = [self objectOrNilForKey:kResponseModelErrorMsg fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.content forKey:kResponseModelContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kResponseModelStatus];
    [mutableDict setValue:self.errorMsg forKey:kResponseModelErrorMsg];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.content = [aDecoder decodeObjectForKey:kResponseModelContent];
    self.status = [aDecoder decodeDoubleForKey:kResponseModelStatus];
    self.errorMsg = [aDecoder decodeObjectForKey:kResponseModelErrorMsg];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_content forKey:kResponseModelContent];
    [aCoder encodeDouble:_status forKey:kResponseModelStatus];
    [aCoder encodeObject:_errorMsg forKey:kResponseModelErrorMsg];
}

- (id)copyWithZone:(NSZone *)zone {
    ResponseModel *copy = [[ResponseModel alloc] init];
    
    
    
    if (copy) {

        copy.content = [self.content copyWithZone:zone];
        copy.status = self.status;
        copy.errorMsg = [self.errorMsg copyWithZone:zone];
    }
    
    return copy;
}


@end
