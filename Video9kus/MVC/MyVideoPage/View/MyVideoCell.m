//
//  MyVideoCell.m
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MyVideoCell.h"

@interface MyVideoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *portraitImgV;

@property (weak, nonatomic) IBOutlet UILabel *userNameLb;

@property (weak, nonatomic) IBOutlet UILabel *timeLb;

@property (weak, nonatomic) IBOutlet UILabel *videoTimeLb;

@property (weak, nonatomic) IBOutlet UIImageView *previewImgV;

@property (weak, nonatomic) IBOutlet UIView *transformV;

@property (weak, nonatomic) IBOutlet UIButton *canPlayBtn;

@property (weak, nonatomic) IBOutlet UIView *transformFailV;

@end

@implementation MyVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark ------------ < Open Method > ------------
- (void)setCellWithModel:(VideoInfoModel *)model{
    
    [self.portraitImgV yy_setImageWithURL:[NSURL URLWithString:UserInfoModelAvatar] placeholder:[UIImage imageWithColor:[UIColor lightGrayColor]] options:0 completion:^(UIImage* _Nullable image, NSURL* _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError* _Nullable error) {
        if (image) {
            self.portraitImgV.image = image;
            if (from & YYWebImageFromRemote)
            {
                self.portraitImgV.alpha = 0;
                [UIView animateWithDuration:0.4 animations:^{self.portraitImgV.alpha = 1;}];
            }
        }
    }];
    
    self.userNameLb.text = UserInfoModelNickName==nil?@"未知用户":UserInfoModelNickName;
    
    self.timeLb.text = [NSString stringWithFormat:@"%@%@",[NSDate unixStamp2TimeIntervalSinceNow:model.addTime],@"前"];
    
    self.videoTimeLb.text = model.vTime;
    
    [self.previewImgV yy_setImageWithURL:[NSURL URLWithString:model.videoImg] placeholder:[UIImage imageWithColor:[UIColor lightGrayColor]] options:0 completion:^(UIImage* _Nullable image, NSURL* _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError* _Nullable error) {
        if (image) {
            self.previewImgV.image = image;
            if (from & YYWebImageFromRemote)
            {
                self.previewImgV.alpha = 0;
                [UIView animateWithDuration:0.4 animations:^{self.previewImgV.alpha = 1;}];
            }
        }
    }];
    
    /// status
    self.canPlayBtn.hidden = YES;
    self.transformV.hidden = YES;
    self.transformFailV.hidden = YES;
    NSInteger s = model.transformStatus;
    switch (s) {
        case 0: /// 'being transformed'
        {
            self.transformV.hidden = NO;
        }
            break;
        case 1: /// 'transform success'
        {
            self.canPlayBtn.hidden = NO;
        }
            break;
        case 2: /// 'transform fail'
        {
            self.transformFailV.hidden = NO;
        }
            break;
        default:
            break;
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
