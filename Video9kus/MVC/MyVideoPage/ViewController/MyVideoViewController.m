//
//  MyVideoViewController.m
//  Video9kus
//
//  Created by Lincal on 2017/4/6.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "MyVideoViewController.h"
#import "MyVideoCell.h"
#import "HorVideoPlayViewController.h"
#import <MJRefresh.h>
#import "MMDRefresher.h"
#import "MMDVideoTransformChecker.h"

/// notification name
#define MMDVideoTransformCheckerNotiName @"MMDMMDVideoTransformCheckerNotiName"

@interface MyVideoViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong) UIImageView * imgTipsV;

/// choose menu
@property (weak, nonatomic) IBOutlet UIView *choiceV;

/// data
@property (nonatomic,strong) NSMutableArray * dataArr;

/// current cell
@property (nonatomic,strong) MyVideoCell * currentCell;

@end

@implementation MyVideoViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.dataArr = [[NSMutableArray alloc]init];
    [self setupUI];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"icon_return_black"] style:UIBarButtonItemStylePlain target:self action:@selector(naviLeftBarButtonClick:)];
    
    /// auto
    [self.tableView.mj_header beginRefreshing];
    [[MMDVideoTransformChecker sharedMMDVideoTransformChecker]startChecker];
    
    /// observer
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationVideoCheck:) name:MMDVideoTransformCheckerNotiName object:nil];
    
}

- (void)setupUI{
    setWeakSelf;
    _tableView.mj_header = [MMDRefresher defaultGifHeaderWithBlock:^{
        [weakSelf requestForList];
    }];;
    
    _tableView.tableFooterView = [[UIView alloc]init];
    
    float w = 230;
    self.imgTipsV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW - w)/2, (ScreenH - w)/2 - 130, w, w)];
    self.imgTipsV.userInteractionEnabled = YES;
    [self.imgTipsV addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTipsViewTap:)]];
    [_tableView addSubview:self.imgTipsV];
    
}

#pragma mark ------------ < Web Request > ------------
- (void)requestForList{
    
    NSDictionary * p  = @{
                          @"encryptId":UserInfoModelEncryptID,
                          };
    
    setWeakSelf;
    [MMDRequester mmd_videoListWithDic:p success:^(ResponseModel *  _Nonnull res) {
        
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.dataArr removeAllObjects];
        
        NSArray * arr = (NSArray *)[MMDNetworkManager jsonStrToContainers:res.content];
        if (arr.count==0) {
            [weakSelf showTipsWithImage:[UIImage imageNamed:@"kong1"]];
        }else{
            [weakSelf hideTips];
        }
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            VideoInfoModel * m = [VideoInfoModel modelObjectWithDictionary:obj];
            [weakSelf.dataArr addObject:m];
            if (idx==arr.count-1) [weakSelf.tableView reloadData];
        }];
        
    } failure:^(NSError * _Nonnull err) {
        
        if (err.code == -1009 && self.dataArr.count==0) {
            [weakSelf showTipsWithImage:[UIImage imageNamed:@"kong2"]];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        
    }];
    
}

- (void)requestForDeleteVideoWithVideoCell:(MyVideoCell *)cell{

    NSIndexPath * idx = [self.tableView indexPathForCell:cell];
    VideoInfoModel * model = [self.dataArr objectAtIndex:idx.row];
    
    NSDictionary * p = @{
                         @"encryptId":UserInfoModelEncryptID,
                         @"video_id" : [NSString stringWithFormat:@"%.0lf",model.videoId],
                         };
    
    [MBProgressHUD showCustomLoadingWithMessage:@"删除中..." toView:nil];
    
    setWeakSelf;
    [MMDNetworkManager mmd_GET:kURL(kVideoDelete) parameters:p progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
        
        [MBProgressHUD hideHUD];
        
        [weakSelf.dataArr removeObject:model];
        [weakSelf.tableView deleteRowsAtIndexPaths:@[idx] withRowAnimation:UITableViewRowAnimationLeft];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD hideHUD];
        
    }];
    
}

#pragma mark ------------ < DataSource > ------------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 280;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellID = @"myVideoCellID";
    MyVideoCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    VideoInfoModel * m = self.dataArr[indexPath.row];
    [cell setCellWithModel:m];
    
    return cell;
}

#pragma mark ------------ < Delegate > ------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

#pragma mark ------------ < Interior Method> ------------
- (void)hideChoiceView{
    [UIView animateWithDuration:0.3 animations:^{
        self.choiceV.alpha = 0;
        self.choiceV.userInteractionEnabled = NO;
    }];
}

- (void)showChoiceView{
    [UIView animateWithDuration:0.3 animations:^{
        self.choiceV.alpha = 1;
        self.choiceV.userInteractionEnabled = YES;
    }];
}

- (NSIndexPath *)getIndexPathFromView:(UIView *)view{
    MyVideoCell * cell = (MyVideoCell *)view.superview.superview;
    if ([cell isKindOfClass:[MyVideoCell class]] && cell!=nil) {
        NSIndexPath * idx = [self.tableView indexPathForCell:cell];
        NSLog(@"LINCALTEST Index %ld",idx.row);
        return idx;
    }
    return nil;
}

- (void)showTipsWithImage:(UIImage *)img{
    self.imgTipsV.image = img;
    self.imgTipsV.userInteractionEnabled = YES;
    self.imgTipsV.hidden = NO;
}

- (void)hideTips{
    self.imgTipsV.userInteractionEnabled = NO;
    self.imgTipsV.hidden = YES;
}

#pragma mark ------------ < Event > ------------
- (IBAction)choiceDeleteButton:(UIButton *)sender {
    
    [self hideChoiceView];
    
    UIAlertAction * aR = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction * aL = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self requestForDeleteVideoWithVideoCell:self.currentCell];
    }];
    UIAlertController * aC = [UIAlertController alertControllerWithTitle:nil message:@"删除视频" preferredStyle:UIAlertControllerStyleAlert];
    [aC addAction:aR];
    [aC addAction:aL];
    [self presentViewController:aC animated:YES completion:nil];
}

- (IBAction)choiceCancleButton:(UIButton *)sender {
    [self hideChoiceView];
}

- (IBAction)choiceViewDidTap:(UITapGestureRecognizer *)sender {
    [self hideChoiceView];
}

- (IBAction)moreButtonClick:(UIButton *)sender {
    [self showChoiceView];
    
    NSIndexPath * idx = [self getIndexPathFromView:sender];
    MyVideoCell * c = [self.tableView cellForRowAtIndexPath:idx];
    self.currentCell = c;
    
}

- (IBAction)playBigButtonClick:(UIButton *)sender {
    
    MyVideoCell * cell = (MyVideoCell *)[[sender superview] superview];
    if ([cell isKindOfClass:[MyVideoCell class]] && cell) {
       
        NSIndexPath * idx = [self.tableView indexPathForCell:cell];
        VideoInfoModel * m = self.dataArr[idx.row];
        
        NSURL * playURL = [NSURL URLWithString:m.videoPath];
        
        HorVideoPlayViewController * v = [[UIStoryboard storyboardWithName:@"StyleSelectViewController" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"HorVideoPlayVCID"];
        [v createVideoViewWithUrlPath:playURL];
        [self.navigationController presentViewController:v animated:YES completion:nil];
        
    }

}

- (IBAction)transformFailViewClick:(UITapGestureRecognizer *)sender {
    DLOG(@"");
}

-(void)imgTipsViewTap:(UITapGestureRecognizer *)tap{
    [self.tableView.mj_header beginRefreshing];
}

-(void)naviLeftBarButtonClick:(UIBarButtonItem *)item{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)notificationVideoCheck:(NSNotification *)n{
    [self requestForList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
