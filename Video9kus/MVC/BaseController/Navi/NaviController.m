//
//  NaviController.m
//  Video9kus
//
//  Created by Lincal on 16/10/16.
//  Copyright © 2016年 Lincal. All rights reserved.
//

#import "NaviController.h"

@interface NaviController ()

@end

@implementation NaviController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.tintColor =[UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
