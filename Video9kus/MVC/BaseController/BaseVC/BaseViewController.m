//
//  BaseViewController.m
//  Video9kus
//
//  Created by Lincal on 16/10/16.
//  Copyright © 2016年 Lincal. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)dismissBack;
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)presentNextVC:(id)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)popBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushNextVCAndHideTabbar:(id)viewController
{
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
