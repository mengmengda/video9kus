//
//  BaseViewController.h
//  Video9kus
//
//  Created by Lincal on 16/10/16.
//  Copyright © 2016年 Lincal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 * dismiss返回上一层
 */
- (void)dismissBack;


/**
 * 弹出下一VC
 *
 * @param viewController 下一VC
 */
- (void)presentNextVC:(id)viewController;


/**
 *  pop返回上一层
 */
- (void)popBack;


/**
 *  push跳转去下一个VC并隐藏Tabbar
 *
 *  @param viewController 下一VC
 */
- (void)pushNextVCAndHideTabbar:(id)viewController;

@end
