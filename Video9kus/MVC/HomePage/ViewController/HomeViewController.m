//
//  HomeViewController.m
//  Video9kus
//
//  Created by Lincal on 17/3/25.
//  Copyright © 2017年 Lincal. All rights reserved.
//

#import "HomeViewController.h"
#import "MMDGIVideoRecordView.h"
#import "MMDVideoToolView.h"

@interface HomeViewController ()

/// Recorder
@property (nonatomic,strong) MMDGIVideoRecordView * videoRecordV;

/// Tool View
@property (nonatomic,strong) MMDVideoToolView * toolV;

/// focus imageView
@property(nonatomic,strong)CALayer * focuslayer;

@end

@implementation HomeViewController

-(void)dealloc{
    [self.videoRecordV removeObserver:self forKeyPath:@"recordedTime"];
}

- (void)awakeFromNib{
    [super awakeFromNib];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self.videoRecordV startVideoSession];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self.videoRecordV stopVideoSeesion];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self requestForVisitorLogin];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupVideoView];
    [self setupUI];

}

#pragma mark ------------ < Interior Method > ------------
- (void)setupVideoView{
    
    /// init videoRecordView
    self.videoRecordV = [[MMDGIVideoRecordView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH)];
    [self.view addSubview:self.videoRecordV];
    
    /// Event
    setWeakSelf;
    self.videoRecordV.MMDGIVideoRecordMaxTimeEvent = ^(){
        // change UI
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.toolV.bottomMiddleBtn.selected = NO;
            [weakSelf.toolV.progressV resetProgress];
        });
    };
    
    self.videoRecordV.MMDGIVideoRecordCompletedEvent = ^{
        // show hud
        [MBProgressHUD showCustomLoadingWithMessage:@"保存中..." toView:nil];
        // wait then push next vc
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUD];
            [weakSelf pushToStyleSelectVC];
        });
    };
    
    /// observe
    [self.videoRecordV addObserver:self forKeyPath:@"recordedTime" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)setupUI{
    
    /// init ToolView
    self.toolV = [MMDVideoToolView DefaultView];
    [self.view addSubview:self.toolV];
    
    /// Event
    setWeakSelf;
    self.toolV.MMDVideoToolViewBtnEvent = ^(MMDVideoToolViewBtnType type, UIButton *btn) {
        BOOL bs = btn.selected;
        switch (type) {
            case MMDBtnTypeTopFirstBtn:
            {
                if (bs) {
                    weakSelf.videoRecordV.position = MMDGICaptureDevicePositionFront;
                }else{
                    weakSelf.videoRecordV.position = MMDGICaptureDevicePositionBack;
                }
            }
                break;
            case MMDBtnTypeTopSecendBtn:
            {
                [weakSelf pushToMyVideoVC];
            }
                break;
            case MMDBtnTypeBottomMiddleBtn:
            {
                if (bs) {
                    [weakSelf.videoRecordV startVideoRecord];
                }else{
                    [weakSelf.videoRecordV stopVideoRecord];
                    //Change UI
                    [weakSelf.toolV.progressV resetProgress];
                }
            }
                break;
            default:
                break;
        }
    };
    
}


- (void)pushToStyleSelectVC{
    
    UIViewController * v = [[UIStoryboard storyboardWithName:@"StyleSelectViewController" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"StyleSelectVC"];
    [self.navigationController pushViewController:v animated:YES];
    
}

- (void)pushToMyVideoVC{
    
    if (self.videoRecordV.isRecording) {
        [MBProgressHUD showError:@"录制未完成"];
        return;
    }
    
    UIViewController * v = [[UIStoryboard storyboardWithName:@"MyVideoViewController" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"MyVideoVC"];
    [self.navigationController pushViewController:v animated:YES];
    
}

- (CALayer *)focuslayer{
    
    if (!_focuslayer) {
        float w = 70;
        
        UIImage * img = [UIImage imageNamed:@"icon_focus"];
        UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, w, w)];
        imgV.image = img;
        
        CALayer *layer = imgV.layer;
        layer.hidden = YES;
        
        [self.view.layer addSublayer:layer];
        _focuslayer = layer;
    }
    
    return _focuslayer;
}

- (void)showFocusImageViewWithPoint:(CGPoint)point{
    
    CALayer *fL = self.focuslayer;
    fL.hidden = NO;
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [fL setPosition:point];
    fL.transform = CATransform3DMakeScale(2.0f,2.0f,1.0f);
    [CATransaction commit];
    
    CABasicAnimation * a = [ CABasicAnimation animationWithKeyPath: @"transform" ];
    a.toValue = [ NSValue valueWithCATransform3D: CATransform3DMakeScale(1.0f,1.0f,1.0f)];
    a.duration = 0.3f;
    a.repeatCount = 1;
    a.removedOnCompletion = NO;
    a.fillMode = kCAFillModeForwards;
    [fL addAnimation: a forKey:@"animation"];
    
    /// target
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self focusLayerNormal];
    });
    
}

- (void)focusLayerNormal {
    self.focuslayer.hidden = YES;
}

#pragma mark ------------ < Web Request > ------------
- (void)requestForLogin{
    NSDictionary * p = @{
                         @"encryptId": UserInfoModelEncryptID,
                         };
    [MMDRequester mmd_loginWithDic:p success:nil failure:nil];
}

- (void)requestForVisitorLogin{
    
    NSDictionary * d = [[NSUserDefaults standardUserDefaults]objectForKey:MMDUserInfoModelKey];
    NSString * s = d[@"encryptId"];
    if (s && s.length!=0) return;
    
    NSDictionary * p = @{
                         @"UUID":MMDUserUDID,
                         };
    [MMDRequester mmd_visitorLoginWithDic:p success:nil failure:nil];
}

#pragma mark ------------ < Event > ------------
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context{
    
    /// Record Time
    if ([keyPath isEqualToString:@"recordedTime"]) {
        
        NSNumber * numTime = (NSNumber *)[change objectForKey:@"new"];
        float time = [numTime floatValue];
        if (time <= 0) return;
        
        // Change UI time Botton
        NSString * ts = [NSString stringWithFormat:@"%02li:%02li",lround(floor(time/60.f)),lround(floor(time/1.f))%60];
        [self.toolV.bottomMiddleBtn setTitle:ts forState:UIControlStateNormal];
        
        // Change UI progress View
        float per = [numTime floatValue] / kMMDGIVideoRecordMaxRT;
        [self.toolV.progressV updateProgressWithValue:per];

    }
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if (touches.count>1 || touches.count == 0) return;
    
    /// get location
    UITouch * t = [touches anyObject];
    CGPoint p = [t locationInView:self.view];
    
    /// show focus image
    [self showFocusImageViewWithPoint:p];

    /// set focus
    [self.videoRecordV focusWithPoint:p];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
