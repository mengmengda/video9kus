//
//  StyleCellModel.h
//  Video9kus
//
//  Created by Lincal on 2017/4/20.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StyleCellModel : NSObject

@property (nonatomic,copy) NSString * imageName;

@property (nonatomic,assign) BOOL isSeletecd;

@end
