//
//  StyleSelectCell.m
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "StyleSelectCell.h"

@interface StyleSelectCell ()

@property (weak, nonatomic) IBOutlet UIView *seletecdBgV;

@property (weak, nonatomic) IBOutlet UIImageView *styleImgV;

@end

@implementation StyleSelectCell

+(instancetype)DefaultView{
    return [[[NSBundle mainBundle] loadNibNamed:@"StyleSelectCell" owner:nil options:nil]lastObject];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.seletecdBgV.layer.borderWidth = 3;
    self.seletecdBgV.layer.borderColor = [UIColor clearColor].CGColor;
}

-(void)setStyleSeletecd:(BOOL)styleSeletecd{
    
    _styleSeletecd = styleSeletecd;
    
    if (_styleSeletecd) {
        [self beenSeletecd];
    }else{
        [self cancleSeletecd];
    }
    
}

- (void)beenSeletecd{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.seletecdBgV.layer.borderColor = UIColorFromRGB(0xfdda35).CGColor;
    }];
    
}

- (void)cancleSeletecd{
    
    [UIView animateWithDuration:0.3 animations:^{
        self.seletecdBgV.layer.borderColor = [UIColor clearColor].CGColor;
    }];
    
}

#pragma mark ------------ < Open Method > ------------
-(void)setCellWithModel:(StyleCellModel *)model{
 
    self.styleImgV.image = [UIImage imageNamed:model.imageName];
    
    self.styleSeletecd = model.isSeletecd;
    
}

@end
