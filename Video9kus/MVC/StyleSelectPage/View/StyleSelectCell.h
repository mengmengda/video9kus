//
//  StyleSelectCell.h
//  Video9kus
//
//  Created by Lincal on 2017/4/7.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StyleCellModel.h"

@interface StyleSelectCell : UICollectionViewCell

+(instancetype)DefaultView;

@property (nonatomic,assign) BOOL styleSeletecd;

- (void)setCellWithModel:(StyleCellModel *)model;

@end
