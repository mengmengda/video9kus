//
//  HorVideoPlayViewController.m
//  Video9kus
//
//  Created by Lincal on 2017/4/10.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "HorVideoPlayViewController.h"

/// time interval for refresh
const static float kTimerInterval = 0.1f;

/// time interval for hide tool view
const static float kTimerHideToolViewInterval = 3.0f;

@interface HorVideoPlayViewController ()

/// video play controll
@property (weak, nonatomic) IBOutlet UISlider *videoSlider;

@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;

@property (weak, nonatomic) IBOutlet UIButton *volumeBtn;

@property (weak, nonatomic) IBOutlet UIButton *playBigBtn;

@property (weak, nonatomic) IBOutlet UILabel *leftTimeLb;

@property (weak, nonatomic) IBOutlet UILabel *rightTimeLb;

@property (weak, nonatomic) IBOutlet UIButton *playBtn;

/// video view by self create
@property (nonatomic,strong) MMDIJKPlayerView * ownVideoV;

/// timer
@property (nonatomic,strong) NSTimer * videoTimer;

/// hide tool view timer
@property (nonatomic,strong) NSTimer * hideToolTimer;
@property (nonatomic,assign) BOOL isToolViewHide;

@end

@implementation HorVideoPlayViewController

-(void)dealloc{
    if (self.videoTimer) {
        [self.videoTimer invalidate];
        self.videoTimer = nil;
    }
    
    if (self.hideToolTimer) {
        [self.hideToolTimer invalidate];
        self.hideToolTimer = nil;
    }
}

-(void)awakeFromNib{
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    self.view.transform = CGAffineTransformIdentity;
    self.view.transform = CGAffineTransformMakeRotation(M_PI* -1.5);
    self.view.bounds = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
    [UIView commitAnimations];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self hideVolumeSlider];
    [self resetHideToolViewTimer];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    
    CGFloat duration = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    self.view.transform = CGAffineTransformIdentity;
    self.view.transform = CGAffineTransformMakeRotation(M_PI*2);
    self.view.bounds = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    [UIView commitAnimations];
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.videoV) {
        [self.videoV fitFrameWith:CGRectMake(0, 0, ScreenH, ScreenW)];
        [self.view addSubview:self.videoV];
        [self.view sendSubviewToBack:self.videoV];
        [self.videoV startPlayVideo];
    }
    
    /// custom
    [self.videoSlider setThumbImage:[UIImage imageNamed:@"icon_circular"] forState:UIControlStateNormal];
    [self.volumeSlider setThumbImage:[UIImage imageNamed:@"icon_circular"] forState:UIControlStateNormal];
    
}

#pragma mark ------------ < Open Method > ------------
- (void)createVideoViewWithUrlPath:(NSURL *)url{
    
    /// init VideoPlayView
    self.ownVideoV = [[MMDIJKPlayerView alloc]initWithFrame:CGRectMake(0, 0, ScreenH, ScreenW)];
    [self.ownVideoV loadPlayerForOnlineVideoWithURL:url];
    [self.view addSubview:self.ownVideoV];
    [self.view sendSubviewToBack:self.ownVideoV];
    
    /// event
    setWeakSelf;
    self.ownVideoV.MMDIJKPlayerStateEvent = ^(MMDIJKPlayerStateType type) {
        switch (type) {
            case MMDIJKPlayerStateTypeStopped: // Stop
            {
                weakSelf.playBtn.selected = NO;
                weakSelf.playBigBtn.selected = NO;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigBtn.alpha = 1;
                }];
                
                //stop Timer
                [weakSelf stopRefreshVideoTimeLabel];
                
                //fix
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf fixTimeLabel];
                });
            }
                break;
            case MMDIJKPlayerStateTypePlaying: // Playing
            {
                weakSelf.playBtn.selected = YES;
                weakSelf.playBigBtn.selected = YES;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigBtn.alpha = 0;
                }];
                
                // change time label text
                [weakSelf refreshTimeLabel];
                
                //start timer
                [weakSelf startRefreshVideoTimeLabel];
                
            }
                break;
            case MMDIJKPlayerStateTypePaused: // Pause
            {
                weakSelf.playBtn.selected = NO;
                weakSelf.playBigBtn.selected = NO;
                weakSelf.playBigBtn.userInteractionEnabled = YES;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigBtn.alpha = 1;
                }];
                
                //stop Timer
                [weakSelf stopRefreshVideoTimeLabel];
                
                /// judge
                if (weakSelf.videoV.playerIsPlaying) {
                    DLOG(@"warning .the player send a wrong call-back");
                    
                    weakSelf.playBtn.selected = YES;
                    weakSelf.playBigBtn.selected = YES;
                    [UIView animateWithDuration:0.3 animations:^{
                        weakSelf.playBigBtn.alpha = 0;
                    }];
                    
                    [weakSelf startRefreshVideoTimeLabel];
                }
                
            }
                break;
            default:
                break;
        }
    };
    
}

#pragma mark ------------ < Interior Method > ------------
- (void)startRefreshVideoTimeLabel{
    setWeakSelf;
    self.videoTimer = [NSTimer scheduledTimerWithTimeInterval:kTimerInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (!weakSelf.videoTimer) {
            [timer invalidate];
            timer = nil;
        }
        [weakSelf refreshTimeLabel];
    }];
}

- (void)stopRefreshVideoTimeLabel{
    [self.videoTimer invalidate];
    self.videoTimer = nil;
}

- (void)refreshTimeLabel{
    
    NSTimeInterval currentT = self.ownVideoV.player.currentPlaybackTime;
    NSTimeInterval durationT = self.ownVideoV.player.duration ;
    
    durationT = [[NSString stringWithFormat:@"%.0lf",durationT] doubleValue] - 1;
    
    NSString * currentS = [NSString stringWithFormat:@"%02li:%02li",lround(floor(currentT/60.f)),lround(floor(currentT/1.f))%60];
    NSString * durationS = [NSString stringWithFormat:@"%02li:%02li",lround(floor(durationT/60.f)),lround(floor(durationT/1.f))%60];
    
    self.leftTimeLb.text = [NSString stringWithFormat:@"%@",currentS];
    self.rightTimeLb.text = [NSString stringWithFormat:@"%@",durationS];
    
    NSLog(@"LINCALTEST  C =======  %lf  D ========== %lf ++++++++++++  %@ >>>>>>>>>>>>  %@",currentT,durationT,currentS,durationS);
    
    if (!self.videoSlider.selected) {
        [self.videoSlider setValue:(currentT / durationT * self.videoSlider.maximumValue) animated:YES];
    }
    
}

- (void)fixTimeLabel{
    self.videoSlider.value = self.videoSlider.maximumValue;
    self.leftTimeLb.text = [NSString stringWithFormat:@"%@",self.rightTimeLb.text];
}

- (void)resetHideToolViewTimer{
    if (self.hideToolTimer) {
        [self.hideToolTimer invalidate];
        self.hideToolTimer = nil;
    }
    setWeakSelf;
    self.hideToolTimer = [NSTimer scheduledTimerWithTimeInterval:kTimerHideToolViewInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
        [weakSelf hideToolView];
    }];
}

- (void)showVolumeSlider{
    float x = (self.volumeBtn.width - 30) / 2 + self.volumeBtn.x;
    float y = self.volumeBtn.y - 160 - 5;
    float width = 30;
    float height = 160;
    self.volumeSlider.frame = CGRectMake(x, y, width, height);
    self.volumeSlider.alpha = 1;
    self.volumeSlider.userInteractionEnabled = YES;
}

- (void)hideVolumeSlider{
    float x = (self.volumeBtn.width - 30) / 2 + self.volumeBtn.x;
    float y = self.volumeBtn.y;
    float width = 30;
    float height = 0;
    self.volumeSlider.frame = CGRectMake(x, y, width, height);
    self.volumeSlider.alpha = 0;
    self.volumeSlider.userInteractionEnabled = NO;
}

- (void)hideToolView{
    [self.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIView class]] && ![obj isKindOfClass:[MMDIJKPlayerView class]]) {
            if (obj == self.volumeSlider && obj.userInteractionEnabled!=YES) return ;
            if (obj == self.playBigBtn && self.playBigBtn.selected != NO) return;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3 animations:^{
                    obj.alpha = 0;
                    obj.userInteractionEnabled = NO;
                }];
            });
        }
    }];
    self.isToolViewHide = YES;
    
}

- (void)showToolView{
    [self.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIView class]] && ![obj isKindOfClass:[MMDIJKPlayerView class]]) {
            if (obj == self.volumeSlider && obj.userInteractionEnabled!=YES) return ;
            if (obj == self.playBigBtn && self.playBigBtn.selected != NO) return;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3 animations:^{
                    obj.alpha = 1;
                    obj.userInteractionEnabled = YES;
                }];
            });
        }
    }];
    self.isToolViewHide = NO;
}

#pragma mark ------------ < Event > ------------
- (IBAction)backButtonClick:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)playBigButton:(UIButton *)sender {
    if (sender.selected) { /// 'YES' playing state
        [self.ownVideoV pausePlayVideo];
    }else{ /// 'NO' pause state
        [self.ownVideoV startPlayVideo];
    }
    
    sender.selected = !sender.selected;
    [self resetHideToolViewTimer];
}

- (IBAction)sliderPan:(UISlider *)sender {
    sender.selected = YES;
    [self resetHideToolViewTimer];
}

- (IBAction)sliderUpInSide:(UISlider *)sender {
    sender.selected = NO;
    NSTimeInterval timePoint = self.ownVideoV.player.duration * sender.value;
    [self.ownVideoV jumpToTime:timePoint];
    [self resetHideToolViewTimer];
}

- (IBAction)volumeButtonClick:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (sender.selected) { // 'YES' should show itself
        [UIView animateWithDuration:0.3 animations:^{
            [self showVolumeSlider];
        }];
    }else{ // 'NO' should hide itself
        [UIView animateWithDuration:0.3 animations:^{
            [self hideVolumeSlider];
        }];
    }
    [self resetHideToolViewTimer];
}

- (IBAction)volumeSliderPan:(UISlider *)sender {
    [self.ownVideoV setVolumeWithValue:sender.value];
    [self resetHideToolViewTimer];
}

- (IBAction)viewTap:(UITapGestureRecognizer *)sender {
    
}

- (IBAction)playBtnClick:(UIButton *)sender {
    if (sender.selected) { /// 'YES' playing state
        [self.ownVideoV pausePlayVideo];
    }else{ /// 'NO' pause state
        [self.ownVideoV startPlayVideo];
    }
    
    sender.selected = !sender.selected;
    [self resetHideToolViewTimer];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.isToolViewHide){
        [self showToolView];
        [self resetHideToolViewTimer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
