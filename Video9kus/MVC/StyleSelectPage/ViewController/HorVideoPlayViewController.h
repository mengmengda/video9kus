//
//  HorVideoPlayViewController.h
//  Video9kus
//
//  Created by Lincal on 2017/4/10.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDIJKPlayerView.h"

@interface HorVideoPlayViewController : UIViewController

@property (nonatomic,strong) MMDIJKPlayerView * videoV;

/// create own Video view
- (void)createVideoViewWithUrlPath:(NSURL *)url;

@end
