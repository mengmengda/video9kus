//
//  StyleSelectViewController.m
//  Video9kus
//
//  Created by Lincal on 2017/4/6.
//  Copyright © 2017年 MMD. All rights reserved.
//

#import "StyleSelectViewController.h"

#import "MMDIJKPlayerView.h"
#import "StyleSelectCell.h"
#import "HorVideoPlayViewController.h"
#import "StyleCellModel.h"

/// time interval for refresh
const static float kTimerInterval = 0.1f;

@interface StyleSelectViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;

@property (weak, nonatomic) IBOutlet UICollectionView *styleCollectionV;

@property (nonatomic,strong) StyleSelectCell * currentCell;

@property (nonatomic,strong) NSMutableArray * dataArr;

/// video play control
@property (weak, nonatomic) IBOutlet UIView *VideoBgV;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *timeRangeLb;

@property (weak, nonatomic) IBOutlet UIButton *playBigButton;

@property (weak, nonatomic) IBOutlet UISlider *videoSlider;

/// video Play View
@property (nonatomic,strong) MMDIJKPlayerView * videoV;

/// timer
@property (nonatomic,strong) NSTimer * videoTimer;

@end

@implementation StyleSelectViewController

-(void)dealloc{
    if (self.videoTimer) {
        [self.videoTimer invalidate];
        self.videoTimer = nil;
    }
}

-(void)awakeFromNib{
    [super awakeFromNib];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupVideoPlayView];

    /// init data
    [self setupData];
    
    /// register
    UINib *cellNib = [UINib nibWithNibName:@"StyleSelectCell" bundle:nil];
    [self.styleCollectionV registerNib:cellNib forCellWithReuseIdentifier:@"styleCellID"];
    
    /// custom
    UICollectionViewFlowLayout * cLayout = [[UICollectionViewFlowLayout alloc]init];
    float w = (ScreenW - 4 * 20) / 3;
    cLayout.itemSize = CGSizeMake(w, w);
    self.styleCollectionV.collectionViewLayout = cLayout;
    
    [self.videoSlider setThumbImage:[UIImage imageNamed:@"icon_circular"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"icon_return_black"] style:UIBarButtonItemStylePlain target:self action:@selector(naviLeftBarButtonClick:)];
    
}

- (void)setupData{
    NSArray * nArr = @[@"building.jpg",@"comic.jpg"];
    self.dataArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < 2; i++) {
        StyleCellModel * model = [[StyleCellModel alloc]init];
        model.imageName = (NSString *)nArr[i];
        if (i==0) model.isSeletecd = YES;
        [self.dataArr addObject:model];
    }
}

- (void)setupVideoPlayView{
    
    /// init VideoPlayView
    self.videoV = [[MMDIJKPlayerView alloc]initWithFrame:self.VideoBgV.bounds];
    [self.videoV loadPlayerForNativeVideoWithPath:[NSURL fileURLWithPath:VideoTempPath]];
    [self.VideoBgV addSubview:self.videoV];
    [self.VideoBgV sendSubviewToBack:self.videoV];
    
    /// event
    setWeakSelf;
    self.videoV.MMDIJKPlayerStateEvent = ^(MMDIJKPlayerStateType type) {
        switch (type) {
            case MMDIJKPlayerStateTypeStopped: // Stop
            {
                weakSelf.playButton.selected = NO;
                weakSelf.playBigButton.userInteractionEnabled = YES;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigButton.alpha = 1;
                }];
                
                //stop Timer
                [weakSelf stopRefreshVideoTimeLabel];
                
                //fix
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf fixTimeLabel];
                });
            }
                break;
            case MMDIJKPlayerStateTypePlaying: // Playing
            {
                weakSelf.playButton.selected = YES;
                weakSelf.playBigButton.selected = YES;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigButton.alpha = 0;
                }];
                
                // change time label text
                [weakSelf refreshTimeLabel];
                
                //start timer
                [weakSelf startRefreshVideoTimeLabel];
             
            }
                break;
            case MMDIJKPlayerStateTypePaused: // Pause
            {
                weakSelf.playButton.selected = NO;
                weakSelf.playBigButton.userInteractionEnabled = YES;
                [UIView animateWithDuration:0.3 animations:^{
                    weakSelf.playBigButton.alpha = 1;
                }];
                
                //stop Timer
                [weakSelf stopRefreshVideoTimeLabel];
                
                /// judge
                if (weakSelf.videoV.playerIsPlaying) {
                    DLOG(@"warning .the player send a wrong call-back");
                    
                    weakSelf.playButton.selected = YES;
                    weakSelf.playBigButton.selected = YES;
                    [UIView animateWithDuration:0.3 animations:^{
                        weakSelf.playBigButton.alpha = 0;
                    }];
                    
                    [weakSelf startRefreshVideoTimeLabel];
                }
                
            }
                break;
            default:
                break;
        }
    };
    
}

- (void)startRefreshVideoTimeLabel{
    setWeakSelf;
    self.videoTimer = [NSTimer scheduledTimerWithTimeInterval:kTimerInterval repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (!weakSelf.videoTimer) {
            [timer invalidate];
            timer = nil;
        }
        [weakSelf refreshTimeLabel];
    }];
}

- (void)stopRefreshVideoTimeLabel{
    [self.videoTimer invalidate];
    self.videoTimer = nil;
}

- (void)refreshTimeLabel{
    
    NSTimeInterval currentT = self.videoV.player.currentPlaybackTime + 0;
    NSTimeInterval durationT = self.videoV.player.duration;
    
//    NSTimeInterval durationT2 = [[NSString stringWithFormat:@"%.0lf",durationT] doubleValue];
//    if (durationT2 < durationT) {
//        durationT = durationT2;
//        if ((durationT - durationT2)>0.2) currentT -= 0.2;
//    }
    
    NSString * currentS = [NSString stringWithFormat:@"%02li:%02li",lround(floor(currentT/60.f)),lround(floor(currentT/1.f))%60];
    NSString * durationS = [NSString stringWithFormat:@"%02li:%02li",lround(floor(durationT/60.f)),lround(floor(durationT/1.f))%60];
    self.timeRangeLb.text = [NSString stringWithFormat:@"%@ / %@",currentS,durationS];

    NSLog(@"LINCALTEST  C =======  %lf  D ========== %lf ++++++++++++  %@ >>>>>>>>>>>>  %@",currentT,durationT,currentS,durationS);
    
    if (!self.videoSlider.selected) {
        [self.videoSlider setValue:(currentT / durationT * self.videoSlider.maximumValue) animated:YES];
    }
}

- (void)fixTimeLabel{
    self.videoSlider.value = self.videoSlider.maximumValue;
    NSString * s = [self.timeRangeLb.text substringFromIndex:7];
    self.timeRangeLb.text = [NSString stringWithFormat:@"%@ /%@",s,s];
}

#pragma mark ------------ < Web Request > ------------
- (void)requestForUploadVideo{
    
    NSIndexPath * idx = [self.styleCollectionV indexPathForCell:self.currentCell];
    
    NSDictionary * p = @{
                         @"encryptId":UserInfoModelEncryptID,
                         @"style":[NSString stringWithFormat:@"%ld",idx.row]
                         };
    
    [MMDRequester mmd_videoUploadWithDic:p success:nil failure:nil];
    
    [MBProgressHUD showCustomLoadingWithMessage:@"准备上传..." toView:self.view];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [MBProgressHUD hideHUDForView:self.view];
        UIViewController * v = [[UIStoryboard storyboardWithName:@"MyVideoViewController" bundle:[NSBundle mainBundle]]instantiateViewControllerWithIdentifier:@"MyVideoVC"];
        [self.navigationController pushViewController:v animated:YES];
        
    });
    
}

#pragma mark ------------ < Delegate > ------------
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    StyleSelectCell * cell = (StyleSelectCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    StyleCellModel * model = (StyleCellModel *)self.dataArr[indexPath.row];
    
    NSIndexPath * currentIdx = [collectionView indexPathForCell:_currentCell];
    StyleCellModel * currentModel = (StyleCellModel *)self.dataArr[currentIdx.row];
    
//    /// it should use model id to fix
//    if (_currentCell==cell) {
//        _currentCell.styleSeletecd = NO;
//        _currentCell = nil;
//        return;
//    }
    
    _currentCell.styleSeletecd = NO;
    currentModel.isSeletecd = NO;
    
    _currentCell = cell;
    
    _currentCell.styleSeletecd = YES;
    model.isSeletecd = YES;
}

#pragma mark ------------ < DataSource > -----------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellID = @"styleCellID";
    StyleSelectCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    StyleCellModel * m = self.dataArr[indexPath.row];
    [cell setCellWithModel:m];
    
    if (cell.styleSeletecd) _currentCell = cell;
    
    return cell;
}

#pragma mark ------------ < Event > ------------
- (IBAction)playButtonClick:(UIButton *)sender {
    if (sender.selected) { /// 'YES' playing state
        [self.videoV pausePlayVideo];
    }else{ /// 'NO' pause state
        [self.videoV startPlayVideo];
        
        self.playBigButton.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.playBigButton.alpha = 0;
        }];
    }
    
    sender.selected = !sender.selected;
}

- (IBAction)sliderPan:(UISlider *)sender {
    sender.selected = YES;
}

- (IBAction)sliderUpInside:(UISlider *)sender {
    sender.selected = NO;
    NSTimeInterval timePoint = self.videoV.player.duration * sender.value;
    [self.videoV jumpToTime:timePoint];
}

- (IBAction)playBigButtonClick:(UIButton *)sender {
    [self.videoV startPlayVideo];
    self.playButton.selected = YES;
    
    sender.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.3 animations:^{
        sender.alpha = 0;
    }];
}

- (IBAction)bottomBtnClick:(UIButton *)sender {
    if (!self.currentCell) {
        [MBProgressHUD showError:@"请选择一个转换风格"];
        return;
    }
    
    [self requestForUploadVideo];
}

- (void)naviLeftBarButtonClick:(UIBarButtonItem *)item{
    setWeakSelf;
    UIAlertAction * aL = [UIAlertAction actionWithTitle:@"继续" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction * aR = [UIAlertAction actionWithTitle:@"退出" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertController * aC = [UIAlertController alertControllerWithTitle:nil message:@"退出将取消本次转换" preferredStyle:UIAlertControllerStyleAlert];
    [aC addAction:aL];
    [aC addAction:aR];
    [self presentViewController:aC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
